/*  xfibresOptions.h

    Saad Jbabdi, Jennifer McNab , FMRIB Image Analysis Group

    Copyright (C) 1999-2010 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(xfibresOptions_h)
#define xfibresOptions_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"
#include "utils/tracer_plus.h"

namespace Xfibres {

class xfibresOptions {
 public:
  static xfibresOptions& getInstance();
  ~xfibresOptions() { delete gopt; }

  Utilities::Option<bool> verbose;
  Utilities::Option<bool> help;
  Utilities::Option<std::string> logdir;
  Utilities::Option<bool> forcedir;
  Utilities::Option<std::string> datafile;
  Utilities::Option<std::string> maskfile;
  Utilities::Option<std::string> bvecsfile;
  Utilities::Option<std::string> bvalsfile;
  Utilities::Option<std::string> gradampfile;
  Utilities::Option<std::string> graddurfile;
  Utilities::Option<std::string> trfile;
  Utilities::Option<std::string> flipanglefile;
  Utilities::Option<std::string> t1file;
  Utilities::Option<std::string> t2file;
  Utilities::Option<std::string> b1file;
  Utilities::Option<bool>   fixt1t2;
  Utilities::Option<int> nfibres;
  Utilities::Option<float> fudge;
  Utilities::Option<int> njumps;
  Utilities::Option<int> nburn;
  Utilities::Option<int> nburn_noard;
  Utilities::Option<int> sampleevery;
  Utilities::Option<int> updateproposalevery;
  Utilities::Option<int> seed;
  Utilities::Option<bool> no_ard;
  Utilities::Option<bool> all_ard;
  Utilities::FmribOption<bool> f0;
  Utilities::FmribOption<bool> ardf0;

  void parse_command_line(int argc, char** argv,  Utilities::Log& logger);

 private:
  xfibresOptions();
  const xfibresOptions& operator=(xfibresOptions&);
  xfibresOptions(xfibresOptions&);

  Utilities::OptionParser options;

  static xfibresOptions* gopt;

};

 inline xfibresOptions& xfibresOptions::getInstance(){
   if(gopt == NULL)
     gopt = new xfibresOptions();

   return *gopt;
 }

 inline xfibresOptions::xfibresOptions() :
   verbose(std::string("-V,--verbose"), false,
	   std::string("switch on diagnostic messages"),
	   false, Utilities::no_argument),
   help(std::string("-h,--help"), false,
	std::string("display this message"),
	false, Utilities::no_argument),
   logdir(std::string("--ld,--logdir"), std::string("logdir"),
	  std::string("log directory (default is logdir)"),
	  false, Utilities::requires_argument),
   forcedir(std::string("--forcedir"),false,std::string("Use the actual directory name given - i.e. don't add + to make a new directory"),false,Utilities::no_argument),
   datafile(std::string("-k,--data,--datafile"), std::string("data"),
	    std::string("data file"),
	    true, Utilities::requires_argument),
   maskfile(std::string("-m,--mask, --maskfile"), std::string("nodif_brain_mask"),
	    std::string("mask file"),
	    true, Utilities::requires_argument),
   bvecsfile(std::string("-r,--bvecs"), std::string("bvecs"),
	     std::string("b vectors file"),
	     true, Utilities::requires_argument),
   bvalsfile(std::string("-b,--bvals"), std::string("bvals"),
	     std::string("b values file"),
	     true, Utilities::requires_argument),
   gradampfile(std::string("-G,--gradamp"),"",
	       std::string("Gradient amplitudes"),
	       true, Utilities::requires_argument),
   graddurfile(std::string("-d,--graddur"),"",
	       std::string("Gradient durations"),
	       true, Utilities::requires_argument),
   trfile(std::string("-T,--tr"),"",
	  std::string("TRs file"),
	  true, Utilities::requires_argument),
   flipanglefile(std::string("-a,--flipangle"),"",
	  std::string("Flip Angle File file"),
	  true, Utilities::requires_argument),
   t1file(std::string("--T1"),"",
	  std::string("T1 map"),
	  false, Utilities::requires_argument),
   t2file(std::string("--T2"),"",
	  std::string("T2 map"),
	  false, Utilities::requires_argument),
   b1file(std::string("--B1"),"",
	  std::string("B1 map"),
	  false, Utilities::requires_argument),
   fixt1t2(std::string("--fixt1t2"),false,
	   std::string("Fix t1 and t2"),
	   false, Utilities::no_argument),
   nfibres(std::string("--nf,--nfibres"),1,
	   std::string("Maximum number of fibres to fit in each voxel (default 1)"),
	   false,Utilities::requires_argument),
   fudge(std::string("--fudge"),1,
	 std::string("ARD fudge factor"),
	 false,Utilities::requires_argument),
   njumps(std::string("--nj,--njumps"),1250,
	  std::string("Num of jumps to be made by MCMC (default is 1250)"),
	  false,Utilities::requires_argument),
   nburn(std::string("--bi,--burnin"),1000,
	 std::string("Total num of jumps at start of MCMC to be discarded (default is 1000)"),
	 false,Utilities::requires_argument),
   nburn_noard(std::string("--bn,--burnin_noard"),0,
	       std::string("num of burnin jumps before the ard is imposed (default is 0)"),
	       false,Utilities::requires_argument),
   sampleevery(std::string("--se,--sampleevery"),25,
	       std::string("Num of jumps for each sample (MCMC) (default is 25)"),
	       false,Utilities::requires_argument),
   updateproposalevery(std::string("--upe,--updateproposalevery"),40,
		       std::string("Num of jumps for each update to the proposal density std (MCMC) (default is 40)"),
		       false,Utilities::requires_argument),
   seed(std::string("--seed"),8665904,std::string("seed for pseudo random number generator"),
	false,Utilities::requires_argument),
   no_ard(std::string("--noard"),false,std::string("Turn ARD off on all fibres"),
	  false,Utilities::no_argument),
   all_ard(std::string("--allard"),false,std::string("Turn ARD on on all fibres"),
	   false,Utilities::no_argument),
   f0(std::string("--f0"),false,std::string("Add to the model an unattenuated signal compartment"),false,Utilities::no_argument),
   ardf0(std::string("--ardf0"),false,std::string("Use ard on f0"),false,Utilities::no_argument),
   options(std::string("xfibres for DWSSFP"),std::string( "xfibres --help (for list of options)\n"))
     {
       try {
       options.add(verbose);
       options.add(help);
       options.add(logdir);
       options.add(forcedir);
       options.add(datafile);
       options.add(maskfile);
       options.add(bvecsfile);
       options.add(bvalsfile);
       options.add(gradampfile);
       options.add(graddurfile);
       options.add(trfile);
       options.add(flipanglefile);
       options.add(t1file);
       options.add(t2file);
       options.add(b1file);
       options.add(fixt1t2);
       options.add(nfibres);
       options.add(fudge);
       options.add(njumps);
       options.add(nburn);
       options.add(nburn_noard);
       options.add(sampleevery);
       options.add(updateproposalevery);
       options.add(seed);
       options.add(no_ard);
       options.add(all_ard);
       options.add(f0);
       options.add(ardf0);
     }
     catch(Utilities::X_OptionError& e) {
       options.usage();
       std::cerr << std::endl << e.what() << std::endl;
     }
     catch(std::exception &e) {
       std::cerr << e.what() << std::endl;
     }

   }
}

#endif
