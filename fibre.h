/*  Copyright (C) 2005 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef __FIBRE_H_
#define __FIBRE_H_


#include <iostream>
#include "stdlib.h"
#include "cprob/libprob.h"
#include <cmath>
#include "miscmaths/miscprob.h"

using namespace std;
using namespace NEWMAT;
using namespace MISCMATHS;

const float maxfloat=1e10;
const float minfloat=1e-10;
const float maxlogfloat=23;
const float minlogfloat=-23;
namespace FIBRE{


 /////////////////////////////////////////////////////////////////////////////////////////////
 ////Class that represents one anisotropic compartment of the PVM model in an MCMC framework
 ////////////////////////////////////////////////////////////////////////////////////////////
 class Fibre{
    float m_th;                   //Current/candidate MCMC state
    float m_ph;
    float m_f;

    float m_th_prop;              //Standard deviation for Gaussian proposal distributions of parameters
    float m_ph_prop;
    float m_f_prop;

    float m_th_old;               //Last accepted value. If a sample is rejected this value is restored
    float m_ph_old;
    float m_f_old;

    float m_th_prior;             //Priors for the model parameters
    float m_ph_prior;
    float m_f_prior;

    float m_th_old_prior;
    float m_ph_old_prior;
    float m_f_old_prior;

    float m_prior_en;             //Joint Prior
    float m_old_prior_en;
    float m_ardfudge;
    bool m_useard;
    int m_th_acc;
    int m_th_rej;
    int m_ph_acc;
    int m_ph_rej;
    int m_f_acc;
    int m_f_rej;

    ColumnVector m_Signal;        //Vector that stores the signal from the anisotropic compartment during the candidate/current MCMC state
    ColumnVector m_Signal_old;

   const float& m_d;
   const ColumnVector& m_alpha;  //Theta angles of bvecs
   const ColumnVector& m_beta;   //Phi angles of bvecs
   const Matrix& m_bvals;        //b values

   // ssfp params
   const float& m_T1;
   const float& m_T2;
   ColumnVector m_E1,m_E2;
   ColumnVector m_gGG;
   const Matrix& diffGradDur;
   const Matrix& diffGradAmp;
   const Matrix& TR;
   const Matrix& flipAngle;
   float m_B1;

   double A1_f,A1_r,A2_f,A2_r;
   double K_f,K_r,F1_f,F1_r;
   double A2_f_03,A2_r_03;
   double Mminus_f,Mminus_r;
   double b1, b2;
   double s_f,r_bux_f,s_r,r_bux_r;
   double K_top_f,K_top_r,K_bottom_r,K_bottom_f;
   double Mminus_top_f,Mminus_bottom_f,Mminus_top_r,Mminus_bottom_r;
   double flip_mod;

   ColumnVector m_cfm,m_sfm;
   bool m_fixt1t2;


 public:
    //constructors::

    Fibre( const ColumnVector& alpha, const ColumnVector& beta,
	   const Matrix& bvals,const float& d,const float& ardfudge,const bool& useard,
	   const Matrix& _diffGradDur,const Matrix& _diffGradAmp,
	   const Matrix& _TR,const Matrix& _flipAngle,
	   const float& t1,const float& t2,float b1,bool fixt1t2):
      m_ardfudge(ardfudge),m_useard(useard), m_d(d),m_alpha(alpha), m_beta(beta), m_bvals(bvals),
      m_T1(t1),m_T2(t2),diffGradDur(_diffGradDur),diffGradAmp(_diffGradAmp),TR(_TR),flipAngle(_flipAngle)
   {
      m_B1=b1;
      m_fixt1t2=fixt1t2;
      init_ssfp();


      m_th=M_PI/2;
      m_th_old=m_th;
      m_ph=0;
      m_ph_old=m_ph;
      m_f=0.01;
      m_f_old=m_f;
      m_th_prop=0.2;
      m_ph_prop=0.2;
      m_f_prop=0.2;

      m_th_prior=0;
      compute_th_prior();

      m_ph_prior=0;
      compute_ph_prior();

      m_f_prior=0;
      compute_f_prior();

      m_Signal.ReSize(alpha.Nrows());
      m_Signal=0;
      m_Signal_old=m_Signal;

      compute_prior();
      compute_signal();

      m_th_acc=0; m_th_rej=0;
      m_ph_acc=0; m_ph_rej=0;
      m_f_acc=0; m_f_rej=0;


    }
    Fibre(const ColumnVector& alpha, const ColumnVector& beta,
	  const Matrix& bvals,const float& d, const float& ardfudge,const bool& useard,
	  const Matrix& _diffGradDur,const Matrix& _diffGradAmp,
	  const Matrix& _TR,const Matrix& _flipAngle,
	  const float& t1,const float& t2,float b1,bool fixt1t2,
	  const float& th, const float& ph, const float& f) :
      m_th(th), m_ph(ph), m_f(f), m_ardfudge(ardfudge),m_useard(useard),m_d(d), m_alpha(alpha), m_beta(beta), m_bvals(bvals),
      m_T1(t1),m_T2(t2),diffGradDur(_diffGradDur),diffGradAmp(_diffGradAmp),TR(_TR),flipAngle(_flipAngle)
    {
      m_B1=b1;
      m_fixt1t2=fixt1t2;
      init_ssfp();

      m_th_old=m_th;
      m_ph_old=m_ph;
      m_f_old=m_f;
      m_th_prop=0.2;
      m_ph_prop=0.2;
      m_f_prop=0.2;

      m_th_prior=0;
      compute_th_prior();

      m_ph_prior=0;
      compute_ph_prior();

      m_f_prior=0;
      compute_f_prior();

      m_Signal.ReSize(alpha.Nrows());
      m_Signal=0;
      m_Signal_old=m_Signal;

      compute_prior();
      compute_signal();

      m_th_acc=0; m_th_rej=0;
      m_ph_acc=0; m_ph_rej=0;
      m_f_acc=0; m_f_rej=0;

    }
    Fibre(const Fibre& rhs):
      m_d(rhs.m_d), m_alpha(rhs.m_alpha), m_beta(rhs.m_beta), m_bvals(rhs.m_bvals),
      m_T1(rhs.m_T1),m_T2(rhs.m_T2),diffGradDur(rhs.diffGradDur),diffGradAmp(rhs.diffGradAmp),TR(rhs.TR),flipAngle(rhs.flipAngle)
   {

      *this=rhs;
    }


    ~Fibre(){}

   void init_ssfp(){
     float gyro=2.0*M_PI*4258.0;
     m_cfm.ReSize(m_alpha.Nrows());
     m_sfm.ReSize(m_alpha.Nrows());
     m_E1.ReSize(m_alpha.Nrows());
     m_E2.ReSize(m_alpha.Nrows());
     m_gGG.ReSize(m_alpha.Nrows());
     float flip_mod;
     for(int i=1;i<=m_alpha.Nrows();i++){
       //Use measured B1map to modify prescribed flip angle as required
       if (m_B1!=0)
	 {flip_mod=flipAngle(1,i)*m_B1;}
       else
	 {flip_mod=flipAngle(1,i);}

       m_cfm(i)=cos(flip_mod);
       m_sfm(i)=sin(flip_mod);

       m_gGG(i)=gyro*diffGradAmp(1,i)*diffGradDur(1,i)/10.0;
       m_E1(i)=exp(-TR(1,i)/m_T1);
       m_E2(i)=exp(-TR(1,i)/m_T2);

     }


   }


    inline float get_th() const{ return m_th;}
    inline void set_th(const float th){ m_th=th; }

    inline float get_ph() const{ return m_ph;}
    inline void set_ph(const float ph){ m_ph=ph; }

    inline float get_f() const{ return m_f;}
    inline void set_f(const float f){ m_f=f; }


   float get_t1()const{return m_T1;}
   float get_t2()const{return m_T2;}
   float get_b1()const{return m_B1;}
   float get_d()const{return m_d;}


    inline void report() const {
      std::cout << "m_T1: " << m_T1 << std::endl;
      std::cout << "m_T2: " << m_T2 << std::endl;
      std::cout << "m_th: " << m_th << std::endl;
      std::cout << "m_ph: " << m_ph << std::endl;
      std::cout << "m_f: " << m_f << std::endl;
      std::cout << "m_th_prop: " << m_th_prop << std::endl;
      std::cout << "m_ph_prop: " << m_ph_prop << std::endl;
      std::cout << "m_f_prop: " << m_f_prop << std::endl;
      std::cout << "m_th_old: " << m_th_old << std::endl;
      std::cout << "m_ph_old: " << m_ph_old << std::endl;
      std::cout << "m_f_old: " << m_f_old << std::endl;
      std::cout << "m_th_prior: " << m_th_prior << std::endl;
      std::cout << "m_ph_prior: " << m_ph_prior << std::endl;
      std::cout << "m_f_prior: " << m_f_prior << std::endl;
      std::cout << "m_th_old_prior: " << m_th_old_prior << std::endl;
      std::cout << "m_ph_old_prior: " << m_ph_old_prior << std::endl;
      std::cout << "m_f_old_prior: " << m_f_old_prior << std::endl;
      std::cout << "m_prior_en: " << m_prior_en << std::endl;
      std::cout << "m_old_prior_en: " << m_old_prior_en << std::endl;
      std::cout << "m_th_acc: " << m_th_acc << std::endl;
      std::cout << "m_th_rej: " << m_th_rej << std::endl;
      std::cout << "m_ph_acc: " << m_ph_acc << std::endl;
      std::cout << "m_ph_rej: " << m_ph_rej << std::endl;
      std::cout << "m_f_acc: " << m_f_acc << std::endl;
      std::cout << "m_f_rej: " << m_f_rej << std::endl;
    }

    inline const ColumnVector& getSignal() const{
      return m_Signal;
    }

    inline void restoreSignal() {
      m_Signal=m_Signal_old;
    }
    inline void setSignal(const ColumnVector& Signal){
      m_Signal=Signal;
    }

    inline void setSignal(const int i, const float val){
      m_Signal(i)=val;
    }

    inline float get_prior() const{ return m_prior_en;}

   float compute_buxton_aniso(const int&i, const float& angtmp){
     if(!m_fixt1t2){
       m_E1(i)=std::exp(-TR(1,i)/m_T1);
       m_E2(i)=std::exp(-TR(1,i)/m_T2);

     }

     double E1=m_E1(i);
     double E2=m_E2(i);

     b1=m_gGG(i);
     b2=b1;
     b1 *= m_d*b1*TR(1,i);
     b2 *= m_d*b2*diffGradDur(1,i);

     A1_f=std::exp(-b1);
     A1_r=std::exp(-b1*angtmp); //A1 for restricted component

     A2_f=std::exp(-b2);
     A2_r=std::exp(-b2*angtmp); //A2 for restricted component

     A2_f_03=exp(-b2/3.0);
     A2_r_03=exp(-b2*angtmp/3.0);

     s_r=E2*A1_r/A2_r_03/A2_r_03/A2_r_03/A2_r_03*(1-E1*m_cfm(i))+E2/A2_r_03*(m_cfm(i)-1);
     r_bux_r=1 - E1*m_cfm(i)+E2*E2*A1_r*A2_r_03*(m_cfm(i)-E1);
     K_top_r=1-E1*A1_r*m_cfm(i)-E2*E2*A1_r*A1_r/A2_r_03/A2_r_03*(E1*A1_r-m_cfm(i));
     K_bottom_r=(E2*A1_r/A2_r_03/A2_r_03/A2_r_03/A2_r_03)*(1+m_cfm(i))*(1-E1*A1_r);
     K_r=K_top_r/K_bottom_r;
     F1_r=K_r - std::sqrt(K_r*K_r-A2_r*A2_r);

     Mminus_top_r=-(1-E1)*E2/A2_r_03/A2_r_03*(F1_r-E2*A1_r*A2_r_03*A2_r_03)*m_sfm(i);
     Mminus_bottom_r=r_bux_r-F1_r*s_r;
     Mminus_r=fabs(Mminus_top_r/Mminus_bottom_r);

     return(Mminus_r);

   }



    //Adapt the standard deviation of the proposal distributions during MCMC execution
    //to avoid over-rejection/over-acceptance of samples
    inline void update_proposals(){
      m_th_prop*=sqrt(float(m_th_acc+1)/float(m_th_rej+1));
      m_th_prop=min(m_th_prop,maxfloat);
      m_ph_prop*=sqrt(float(m_ph_acc+1)/float(m_ph_rej+1));
      m_ph_prop=min(m_ph_prop,maxfloat);
      m_f_prop*=sqrt(float(m_f_acc+1)/float(m_f_rej+1));
      m_f_prop=min(m_f_prop,maxfloat);
      m_th_acc=0;
      m_th_rej=0;
      m_ph_acc=0;
      m_ph_rej=0;
      m_f_acc=0;
      m_f_rej=0;
    }


    inline bool compute_th_prior(){
      m_th_old_prior=m_th_prior;

      if(m_th==0){m_th_prior=0;}
      else{
	m_th_prior=-log(fabs(sin(m_th)/2));
      }

      return false; //instant rejection flag
    }


    inline bool compute_ph_prior(){
      m_ph_old_prior=m_ph_prior;
      m_ph_prior=0;

      return false;
    }


    inline bool compute_f_prior(bool can_use_ard=true){
      m_f_old_prior=m_f_prior;
      if ((m_f<=0) || (m_f>=1) )
	return true;
      else{
	if(!can_use_ard)
	  m_f_prior=0;
	else{
	  if(m_useard)
	    m_f_prior=std::log(m_f);
	  else
	    m_f_prior=0;
	}
	m_f_prior=m_ardfudge*m_f_prior;
	return false;
      }
    }


    inline void compute_prior(){
      m_old_prior_en=m_prior_en;
      m_prior_en=m_th_prior+m_ph_prior+m_f_prior;
    }


    //Compute model predicted signal, only due to the anisotropic compartment
    void compute_signal(){
       m_Signal_old=m_Signal;

       for (int i = 1; i <= m_alpha.Nrows(); i++){
	 float cos_alpha_minus_theta=cos(m_alpha(i)-m_th);   //Used trigonometric identities to reduce the number of sinusoids evaluated
	 float cos_alpha_plus_theta=cos(m_alpha(i)+m_th);

	 float angtmp=cos(m_ph-m_beta(i))*(cos_alpha_minus_theta-cos_alpha_plus_theta)/2 + (cos_alpha_minus_theta+cos_alpha_plus_theta)/2;
	 angtmp=angtmp*angtmp;

	 // this is the bit that needs changing
	 m_Signal(i)=compute_buxton_aniso(i,angtmp); //exp(-m_d*m_bvals(1,i)*angtmp);
       }
       //OUT(m_Signal.t());
    }


    inline bool propose_th(){
      m_th_old=m_th;
      m_th+=normrnd().AsScalar()*m_th_prop;
      bool rejflag=compute_th_prior();//inside this it stores the old prior
      compute_prior();
      compute_signal();
      return rejflag;
    };


    inline void accept_th(){
      m_th_acc++;
    }


    inline void reject_th(){
      m_th=m_th_old;
      m_th_prior=m_th_old_prior;
      m_prior_en=m_old_prior_en;
      m_Signal=m_Signal_old;//Is there a better way of doing this??
      m_th_rej++;
    }


    inline bool propose_ph(){
      m_ph_old=m_ph;
      m_ph+=normrnd().AsScalar()*m_ph_prop;
       bool rejflag=compute_ph_prior();//inside this it stores the old prior
      compute_prior();
      compute_signal();
      return rejflag;
    };


    inline void accept_ph(){
      m_ph_acc++;
    }


    inline void reject_ph(){
      m_ph=m_ph_old;
      m_ph_prior=m_ph_old_prior;
      m_prior_en=m_old_prior_en;
      m_Signal=m_Signal_old;//Is there a better way of doing this??
      m_ph_rej++;
    }


    bool propose_th_ph(float th,float ph){
      m_th=th;m_ph=ph;
      bool rejflag_th=compute_th_prior();//inside this it stores the old prior
      bool rejflag_ph=compute_ph_prior();

      compute_prior();
      compute_signal();

      return rejflag_th | rejflag_ph;
    }


    void accept_th_ph(){
      m_th_acc++;
      m_ph_acc++;
    }


    void reject_th_ph(){
      m_ph=m_ph_old;
      m_ph_prior=m_ph_old_prior;

      m_th=m_th_old;
      m_th_prior=m_th_old_prior;

      m_prior_en=m_old_prior_en;
      m_Signal=m_Signal_old;//Is there a better way of doing this??

      m_th_rej++;
      m_ph_rej++;
    }


    inline bool propose_f( bool can_use_ard=true){
      m_f_old=m_f;
      m_f+=normrnd().AsScalar()*m_f_prop;
      bool rejflag=compute_f_prior(can_use_ard);
      compute_prior();
      return rejflag;
    };


    inline void accept_f(){
      m_f_acc++;
    }


    inline void reject_f(){
      m_f=m_f_old;
      m_f_prior=m_f_old_prior;
      m_prior_en=m_old_prior_en;
      m_f_rej++;
    }



    Fibre& operator=(const Fibre& rhs){
      m_th=rhs.m_th;
      m_ph=rhs.m_ph;
      m_f=rhs.m_f;
      m_th_prop=rhs.m_th_prop;
      m_ph_prop=rhs.m_ph_prop;
      m_f_prop=rhs.m_f_prop;
      m_th_old=rhs.m_th_old;
      m_ph_old=rhs.m_ph_old;
      m_f_old=rhs.m_f_old;
      m_th_prior=rhs.m_th_prior;
      m_ph_prior=rhs.m_ph_prior;
      m_f_prior=rhs.m_f_prior;
      m_th_old_prior=rhs.m_th_old_prior;
      m_ph_old_prior=rhs.m_ph_old_prior;
      m_f_old_prior=rhs.m_f_old_prior;
      m_prior_en=rhs.m_prior_en;
      m_old_prior_en=rhs.m_old_prior_en;
      m_th_acc=rhs.m_th_acc;
      m_th_rej=rhs.m_th_rej;
      m_ph_acc=rhs.m_ph_acc;
      m_ph_rej=rhs.m_ph_rej;
      m_f_acc=rhs.m_f_acc;
      m_f_rej=rhs.m_f_rej;
      m_Signal=rhs.m_Signal;
      m_Signal_old=rhs.m_Signal_old;
      m_ardfudge=rhs.m_ardfudge;
      m_useard=rhs.m_useard;
      //ssfp
      m_fixt1t2=rhs.m_fixt1t2;
      m_B1=rhs.m_B1;
      m_E1=rhs.m_E1;
      m_E2=rhs.m_E2;
      m_gGG=rhs.m_gGG;
      m_cfm=rhs.m_cfm;
      m_sfm=rhs.m_sfm;

      return *this;
    }

    friend  ostream& operator<<(ostream& ostr,const Fibre& p);

  };

//overload <<
  inline ostream& operator<<(ostream& ostr,const Fibre& p){
    ostr<<p.m_th<<" "<<p.m_ph<<" "<<p.m_f<<endl;
    return ostr;
  }


  /////////////////////////////////////////////////////////////////////////////////////////////////////
  //Class that represents the PVM model in an MCMC framework. An isotropic compartment and M>=1
  //anisotropic compartments (Fibre objects) are considered. Functions are defined for performing
  //a single MCMC iteration
  /////////////////////////////////////////////////////////////////////////////////////////////////////
  class Multifibre{
    vector<Fibre> m_fibres;
    float m_d;
    float m_d_old;
    float m_d_prop;
    float m_d_prior;
    float m_d_old_prior;
    float m_d_acc;
    float m_d_rej;

    float m_S0;
    float m_S0_old;
    float m_S0_prop;
    float m_S0_prior;
    float m_S0_old_prior;
    float m_S0_acc;
    float m_S0_rej;

    float m_f0;                  //Volume fraction for the unattenuated signal compartment
    float m_f0_old;
    float m_f0_prop;
    float m_f0_prior;
    float m_f0_old_prior;
    float m_f0_acc;
    float m_f0_rej;

    float m_prior_en;             //Joint Prior
    float m_old_prior_en;
    float m_likelihood_en;        //Likelihood
    float m_old_likelihood_en;
    float m_energy;               //Posterior
    float m_old_energy;
    float m_ardfudge;
    ColumnVector m_iso_Signal;    //Vector that stores the signal from the isotropic compartment during the candidate/current state
    ColumnVector m_iso_Signal_old;



    const ColumnVector& m_data;   //Data vector
    const ColumnVector& m_alpha;  //Theta angles of bvecs
    const ColumnVector& m_beta;   //Phi angles of bvecs
    const Matrix& m_bvals;        //b values

    const bool m_includef0;       //If true, include an unattenuated signal compartment in the model with fraction f0
    const bool m_ardf0;           //If true, use ard on the f0 compartment


   // ssfp params
   float m_T1,m_T2;
   ColumnVector m_E1,m_E2;
   ColumnVector m_gGG;
   const Matrix& diffGradDur;
   const Matrix& diffGradAmp;
   const Matrix& TR;
   const Matrix& flipAngle;
   float m_B1;
    ColumnVector m_cfm,m_sfm;
    bool m_fixt1t2;

   double A1_f,A1_r,A2_f,A2_r;
   double K_f,K_r,F1_f,F1_r;
   double A2_f_03,A2_r_03;
   double Mminus_f,Mminus_r;
   double b1, b2;
   double s_f,r_bux_f,s_r,r_bux_r;
   double K_top_f,K_top_r,K_bottom_r,K_bottom_f;
   double Mminus_top_f,Mminus_bottom_f,Mminus_top_r,Mminus_bottom_r;
   double flip_mod;


    float m_t1_old;
    float m_t1_prop;
    float m_t1_prior;
    float m_t1_old_prior;
    float m_t1_acc;
    float m_t1_rej;

    float m_t2_old;
    float m_t2_prop;
    float m_t2_prior;
    float m_t2_old_prior;
    float m_t2_acc;
    float m_t2_rej;

    float m_pm_T1,m_pv_T1,m_min_T1,m_max_T1;
    float m_pm_T2,m_pv_T2,m_min_T2,m_max_T2;

  public:
    //Constructor
    Multifibre(const ColumnVector& data,const ColumnVector& alpha,
	       const ColumnVector& beta, const Matrix& b, int N ,
	       const Matrix& _diffGradDur,const Matrix& _diffGradAmp,const Matrix& _TR,const Matrix& _flipAngle,bool fixt1t2,
	       float ardfudge=1, bool inclf0=false, bool ardf0=false):
      m_ardfudge(ardfudge),m_data(data), m_alpha(alpha), m_beta(beta), m_bvals(b),
      m_includef0(inclf0), m_ardf0(ardf0),diffGradDur(_diffGradDur),diffGradAmp(_diffGradAmp),TR(_TR),flipAngle(_flipAngle){

      m_iso_Signal.ReSize(alpha.Nrows());
      m_iso_Signal=0;
      m_iso_Signal_old=m_iso_Signal;            //Initialize vectors that keep the signal from the isotropic compartment

      m_d_acc=0; m_d_rej=0;
      m_S0_acc=0; m_S0_rej=0;
      m_f0_acc=0; m_f0_rej=0;
      m_d_prior=0; m_S0_prior=0; m_f0_prior=0;
      m_d=0; m_d_old=0; m_S0=0; m_S0_old=0;
      m_f0=0.0; m_f0_old=0.0;   //Set this parameter to 0, it will be initialized later only if m_includef0 is true


      m_t1_acc=0;m_t1_rej=0;
      m_t2_acc=0;m_t2_rej=0;
      m_t1_prior=0;m_t2_prior=0;
      m_t1_old=0;m_t2_old=0;

      m_T1=833.0/1000.0; // in seconds
      m_T2=80.0/1000.0;
      m_B1=1;
      m_fixt1t2=fixt1t2;
      init_ssfp();

      // hard coded mean/variance/min/max
      m_pm_T1=833.0/1000.0;
      m_pv_T1=80.0/1000.0;
      m_pv_T1*=m_pv_T1;
      m_min_T1=670/1000.0;
      m_max_T1=1000.0/1000.0;

      m_pm_T2=65.0/1000.0;
      m_pv_T2=6.5/1000.0;
      m_pv_T2*=m_pv_T2;
      m_min_T2=52/1000.0;
      m_max_T2=78.0/1000.0;


   }

    ~Multifibre(){}


   void init_ssfp(){
     float gyro=2.0*M_PI*4258.0;
     m_cfm.ReSize(m_alpha.Nrows());
     m_sfm.ReSize(m_alpha.Nrows());
     m_E1.ReSize(m_alpha.Nrows());
     m_E2.ReSize(m_alpha.Nrows());
     m_gGG.ReSize(m_alpha.Nrows());
     float flip_mod;
     for(int i=1;i<=m_alpha.Nrows();i++){
       //Use measured B1map to modify prescribed flip angle as required
       if (m_B1!=0)
	 {flip_mod=flipAngle(1,i)*m_B1;}
       else
	 {flip_mod=flipAngle(1,i);}

       m_cfm(i)=cos(flip_mod);
       m_sfm(i)=sin(flip_mod);

       m_gGG(i)=gyro*diffGradAmp(1,i)*diffGradDur(1,i)/10.0;
       m_E1(i)=exp(-TR(1,i)/m_T1);
       m_E2(i)=exp(-TR(1,i)/m_T2);

     }
   }

    const vector<Fibre>& fibres() const{
      return m_fibres;
    }

    vector<Fibre>& get_fibres(){
      return m_fibres;
    }

    void addfibre(const float th, const float ph, const float f,const bool use_ard=true){
      Fibre fib(m_alpha,m_beta,
		m_bvals,m_d,m_ardfudge,use_ard,
		diffGradDur,diffGradAmp,
		TR,flipAngle,
		m_T1,m_T2,m_B1,m_fixt1t2,
		th,ph,f);
       m_fibres.push_back(fib);
    }

    void addfibre(){
      Fibre fib(m_alpha,m_beta,
		m_bvals,m_d,m_ardfudge,true,
		diffGradDur,diffGradAmp,
		TR,flipAngle,
		m_T1,m_T2,m_B1,m_fixt1t2);
      m_fibres.push_back(fib);
    }

    void initialise_energies(){
      compute_d_prior();
      if (m_includef0)
	compute_f0_prior();
      compute_S0_prior();
      if(!m_fixt1t2){
	compute_t1_prior();
	compute_t2_prior();
      }

      m_prior_en=0;
      compute_prior();
      m_likelihood_en=0;
      compute_iso_signal();
      compute_likelihood();
      compute_energy();
    }


    //Initialize standard deviations for the proposal distributions
    void initialise_props(){
      m_S0_prop=m_S0/10.0; //must have set initial values before this is called;
      m_d_prop=m_d/10.0;
      m_f0_prop=0.2;

      m_t1_prop=m_T1/10.0;
      m_t2_prop=m_T2/10.0;
    }


    inline float get_d() const{ return m_d;}
    inline void set_d(const float d){ m_d=d;compute_iso_signal();}

    inline int get_nfibre(){return m_fibres.size();}

    inline float get_energy() const { return m_likelihood_en+m_prior_en;}
    inline float get_likelihood_energy() const { return m_likelihood_en;}
    inline float get_prior() const {return m_prior_en;}

    inline float get_S0() const{ return m_S0;}
    inline void set_S0(const float S0){ m_S0=S0; }

    inline float get_f0() const{ return m_f0;}
    inline void set_f0(const float f0){ m_f0=f0; }

    void set_t1t2b1(const float& t1,const float& t2,const float& b1=1){
      m_T1=t1;
      m_T2=t2;

      if(!m_fixt1t2){
	if(t1<=m_min_T1 || t1>=m_max_T1)
	  m_T1=m_pm_T1;
	if(t2<=m_min_T2 || t2>=m_max_T2)
	  m_T2=m_pm_T2;
      }
      m_B1=b1;

      init_ssfp();
      compute_iso_signal();
    }
    inline float get_t1() const{ return m_T1;}
    inline float get_t2() const{ return m_T2;}


    inline void report() const{
      std::cout << "m_T1: " << m_T1 << std::endl;
      std::cout << "m_pm_T1: " << m_pm_T1 << std::endl;
      std::cout << "m_pv_T1: " << m_pv_T1 << std::endl;
      std::cout << "m_t1_old: " << m_t1_old << std::endl;
      std::cout << "m_t1_prior: " << m_t1_prior << std::endl;
      std::cout << "m_t1_old_prior: " << m_t1_old_prior << std::endl;
      std::cout << "m_t1_acc: " << m_t1_acc << std::endl;
      std::cout << "m_t1_rej: " << m_t1_rej << std::endl;
      std::cout << "m_T2: " << m_T2 << std::endl;
      std::cout << "m_t2_old: " << m_t2_old << std::endl;
      std::cout << "m_t2_prior: " << m_t2_prior << std::endl;
      std::cout << "m_t2_old_prior: " << m_t2_old_prior << std::endl;
      std::cout << "m_t2_acc: " << m_t2_acc << std::endl;
      std::cout << "m_t2_rej: " << m_t2_rej << std::endl;
      std::cout << "m_d: " << m_d << std::endl;
      std::cout << "m_d_old: " << m_d_old << std::endl;
      std::cout << "m_d_prop: " << m_d_prop << std::endl;
      std::cout << "m_d_prior: " << m_d_prior << std::endl;
      std::cout << "m_d_old_prior: " << m_d_old_prior << std::endl;
      std::cout << "m_d_acc: " << m_d_acc << std::endl;
      std::cout << "m_d_rej: " << m_d_rej << std::endl;
      std::cout << "m_S0: " << m_S0 << std::endl;
      std::cout << "m_S0_old: " << m_S0_old << std::endl;
      std::cout << "m_S0_prop: " << m_S0_prop << std::endl;
      std::cout << "m_S0_prior: " << m_S0_prior << std::endl;
      std::cout << "m_S0_old_prior: " << m_S0_old_prior << std::endl;
      std::cout << "m_S0_acc: " << m_S0_acc << std::endl;
      std::cout << "m_S0_rej: " << m_S0_rej << std::endl;
      std::cout << "m_prior_en: " << m_prior_en << std::endl;
      std::cout << "m_old_prior_en: " << m_old_prior_en << std::endl;
      std::cout << "m_likelihood_en: " << m_likelihood_en << std::endl;
      std::cout << "m_old_likelihood_en: " << m_old_likelihood_en << std::endl;
      std::cout << "m_energy: " << m_energy << std::endl;
      std::cout << "m_old_energy: " << m_old_energy << std::endl;
      for (unsigned int i=0;i<m_fibres.size();i++){cout <<"fibre "<<i<<endl;m_fibres[i].report();}
    }


    inline bool compute_d_prior(){
      m_d_old_prior=m_d_prior;
      if(m_d<0 || m_d>1e16)
	return true;
      else{
	//m_d_prior=0;
	m_d_prior=-3.0*std::log(m_d)+m_d/0.0003;
	return false;
      }
    }

    //More restrictive prior for the model with f0
    //particularly useful for the CSF voxels
    inline bool compute_d_prior_f0(){
      m_d_old_prior=m_d_prior;
      if(m_d<0 || m_d>0.008)
	return true;
      else{
	m_d_prior=0;
	return false;
      }
    }

    inline bool compute_S0_prior(){
      m_S0_old_prior=m_S0_prior;
      if(m_S0<0 || m_S0>100000) return true;
      else
	{
	  m_S0_prior=0;
	  return false;
	}
    }


    inline bool compute_f0_prior(){
      m_f0_old_prior=m_f0_prior;
      if (m_f0<=0 || m_f0>=1)
	return true;
      else{
	if(!m_ardf0)     //Without ARD
	  m_f0_prior=0;
	else              //With ARD
	  m_f0_prior=std::log(m_f0);
	return false;
      }
    }


    inline bool compute_t1_prior(){
      m_t1_old_prior=m_t1_prior;
      if(m_T1<m_min_T1 || m_T1>m_max_T1) return true;
      else
	{
	  //m_t1_prior=0;
	  m_t1_prior=(m_T1-m_pm_T1)*(m_T1-m_pm_T1)/2.0/m_pv_T1;

	  return false;
	}
    }
    inline bool compute_t2_prior(){
      m_t2_old_prior=m_t2_prior;
      if(m_T2<m_min_T2 || m_T2>m_max_T2) return true;
      else
	{
	  //m_t2_prior=0;
	  m_t2_prior=(m_T2-m_pm_T2)*(m_T2-m_pm_T2)/2.0/m_pv_T2;

	  return false;
	}
    }

    //Check if sum of volume fractions is >1
    bool reject_f_sum(){
      float fsum=m_f0;
      for(unsigned int f=0;f<m_fibres.size();f++){
	fsum+=m_fibres[f].get_f();
      }
      return fsum>1; //true if sum(f) > 1 and therefore, we should reject f
    }


    //Compute Joint Prior
    inline void compute_prior(){
      m_old_prior_en=m_prior_en;
      m_prior_en=m_d_prior+m_S0_prior;
      if (m_includef0)
	m_prior_en=m_prior_en+m_f0_prior;
      if (!m_fixt1t2)
	m_prior_en=m_prior_en+m_t1_prior+m_t2_prior;

      for(unsigned int f=0;f<m_fibres.size(); f++){
	m_prior_en=m_prior_en+m_fibres[f].get_prior();
      }
    }


   float compute_buxton_iso(const int&i){


     if(!m_fixt1t2){
       m_E1(i)=exp(-TR(1,i)/m_T1);
       m_E2(i)=exp(-TR(1,i)/m_T2);
     }

     double E1=m_E1(i);
     double E2=m_E2(i);

     b1=m_gGG(i);
     b2=b1;
     b1 *= m_d*b1*TR(1,i);
     b2 *= m_d*b2*diffGradDur(1,i);

     A1_f=std::exp(-b1);
     A2_f=std::exp(-b2);

     A2_f_03=exp(-b2/3.0);

     s_f=E2*A1_f/A2_f_03/A2_f_03/A2_f_03/A2_f_03*(1-E1*m_cfm(i))+E2/A2_f_03*(m_cfm(i)-1);
     r_bux_f=1 - E1*m_cfm(i)+E2*E2*A1_f*A2_f_03*(m_cfm(i)-E1);

     K_top_f= 1-E1*A1_f*m_cfm(i)-E2*E2*A1_f*A1_f/A2_f_03/A2_f_03* (E1*A1_f-m_cfm(i) );

     K_bottom_f=(E2*A1_f/A2_f_03/A2_f_03/A2_f_03/A2_f_03)*(1+m_cfm(i))*(1-E1*A1_f);
     K_f=K_top_f/K_bottom_f;
     F1_f=K_f - std::sqrt(K_f*K_f-A2_f*A2_f);
     Mminus_top_f=-(1-E1)*E2/A2_f_03/A2_f_03*(F1_f-E2*A1_f*A2_f_03*A2_f_03)*m_sfm(i);
     Mminus_bottom_f=r_bux_f-F1_f*s_f;
     Mminus_f=fabs(Mminus_top_f/Mminus_bottom_f);

     return(Mminus_f);

   }


    void compute_iso_signal(){               //Function that computes the signal from the isotropic compartment
      m_iso_Signal_old=m_iso_Signal;

      // another bit that needs changing
      for(int i=1;i<=m_alpha.Nrows();i++)
	m_iso_Signal(i)=compute_buxton_iso(i); //exp(-m_d*m_bvals(1,i));

      //OUT(m_iso_Signal.t());
    }


   void compute_likelihood(){
      m_old_likelihood_en=m_likelihood_en;
      ColumnVector pred(m_alpha.Nrows());
      pred=0;
      float fsum=m_f0;
      for(unsigned int f=0;f<m_fibres.size();f++){   //Signal from the anisotropic compartments
	pred=pred+m_fibres[f].get_f()*m_fibres[f].getSignal();
	fsum+=m_fibres[f].get_f();                   //Total anisotropic volume fraction
      }

      for(int i=1;i<=pred.Nrows();i++){
      	pred(i)=m_S0*(pred(i)+(1-fsum)*m_iso_Signal(i)+m_f0);   //Add the signal from the isotropic compartment
      }
                                                //and multiply by S0 to get the total signal
      float sumsquares=(m_data-pred).SumSquare();             //Sum of squared residuals
      m_likelihood_en=(m_data.Nrows()/2.0)*log(sumsquares/2.0);

   }

    ReturnMatrix get_prediction(){
      ColumnVector pred(m_alpha.Nrows());
      pred=0;
      float fsum=m_f0;
      for(unsigned int f=0;f<m_fibres.size();f++){   //Signal from the anisotropic compartments
	pred=pred+m_fibres[f].get_f()*m_fibres[f].getSignal();
	fsum+=m_fibres[f].get_f();                   //Total anisotropic volume fraction
      }

      for(int i=1;i<=pred.Nrows();i++){
      	pred(i)=m_S0*(pred(i)+(1-fsum)*m_iso_Signal(i)+m_f0);   //Add the signal from the isotropic compartment
      }
                                                //and multiply by S0 to get the total signal

      std::cout << "m_S0: " << m_S0 << std::endl;
      std::cout << "m_f0: " << m_f0 << std::endl;
      std::cout << "m_d: " << m_d << std::endl;
      std::cout << "m_fibres.size(): " << m_fibres.size() << std::endl;
      std::cout << "m_fibres[0].get_th(): " << m_fibres[0].get_th() << std::endl;
      std::cout << "m_fibres[0].get_ph(): " << m_fibres[0].get_ph() << std::endl;
      std::cout << "m_fibres[0].get_f(): " << m_fibres[0].get_f() << std::endl;
      std::cout << "m_T1: " << m_T1 << std::endl;
      std::cout << "m_T2: " << m_T2 << std::endl;
      std::cout << "m_B1: " << m_B1 << std::endl;
      std::cout << "m_fibres[0].get_d(): " << m_fibres[0].get_d() << std::endl;
      std::cout << "m_fibres[0].get_t1(): " << m_fibres[0].get_t1() << std::endl;
      std::cout << "m_fibres[0].get_t2(): " << m_fibres[0].get_t2() << std::endl;
      std::cout << "m_fibres[0].get_b1(): " << m_fibres[0].get_b1() << std::endl;


      pred.Release();
      return pred;
    }

    inline void compute_energy(){
      m_old_energy=m_energy;
      m_energy=m_prior_en+m_likelihood_en;
    }

    //Test whether the candidate state will be accepted
    bool test_energy(){
      float tmp=exp(m_old_energy-m_energy);
      return (tmp>unifrnd().AsScalar());
    }


    inline void restore_energy(){
      m_prior_en=m_old_prior_en;
      m_likelihood_en=m_old_likelihood_en;
      m_energy=m_old_energy;
    }


    inline void restore_energy_no_lik(){
      m_prior_en=m_old_prior_en;
      m_energy=m_old_energy;
    }


    inline bool propose_d(){
      bool rejflag;
      m_d_old=m_d;
      m_d+=normrnd().AsScalar()*m_d_prop;
      if (m_includef0)
      	rejflag=compute_d_prior_f0();
      else
	rejflag=compute_d_prior();
      for(unsigned int f=0;f<m_fibres.size();f++)
	m_fibres[f].compute_signal();
      compute_iso_signal();
      return rejflag;
    };


    inline void accept_d(){
      m_d_acc++;
    }


    inline void reject_d(){
      m_d=m_d_old;
      m_d_prior=m_d_old_prior;
      m_prior_en=m_old_prior_en;
      for(unsigned int f=0;f<m_fibres.size();f++)
	m_fibres[f].restoreSignal();
      m_iso_Signal=m_iso_Signal_old;
      m_d_rej++;
    }


    inline bool propose_S0(){
      m_S0_old=m_S0;
      m_S0+=normrnd().AsScalar()*m_S0_prop;
      bool rejflag=compute_S0_prior();//inside this it stores the old prior
      return rejflag;
    };


    inline void accept_S0(){
      m_S0_acc++;
    }


    inline void reject_S0(){
      m_S0=m_S0_old;
      m_S0_prior=m_S0_old_prior;
      m_prior_en=m_old_prior_en;
      m_S0_rej++;
    }


    inline bool propose_t1(){
      m_t1_old=m_T1;
      m_T1+=normrnd().AsScalar()*m_t1_prop;
      bool rejflag=compute_t1_prior();//inside this it stores the old prior
      for(unsigned int f=0;f<m_fibres.size();f++)
	m_fibres[f].compute_signal();
      compute_iso_signal();
      return rejflag;
    };
    inline void accept_t1(){
      m_t1_acc++;
    }
    inline void reject_t1(){
      m_T1=m_t1_old;
      m_t1_prior=m_t1_old_prior;
      m_prior_en=m_old_prior_en;
      for(unsigned int f=0;f<m_fibres.size();f++)
	m_fibres[f].restoreSignal();
      m_iso_Signal=m_iso_Signal_old;
      m_t1_rej++;
    }
    inline bool propose_t2(){
      m_t2_old=m_T2;
      m_T2+=normrnd().AsScalar()*m_t2_prop;
      bool rejflag=compute_t2_prior();//inside this it stores the old prior
      for(unsigned int f=0;f<m_fibres.size();f++)
	m_fibres[f].compute_signal();
      compute_iso_signal();
      return rejflag;
    };
    inline void accept_t2(){
      m_t2_acc++;
    }
    inline void reject_t2(){
      m_T2=m_t2_old;
      m_t2_prior=m_t2_old_prior;
      m_prior_en=m_old_prior_en;
      for(unsigned int f=0;f<m_fibres.size();f++)
	m_fibres[f].restoreSignal();
      m_iso_Signal=m_iso_Signal_old;
      m_t2_rej++;
    }




    inline bool propose_f0(){
      m_f0_old=m_f0;
      m_f0+=normrnd().AsScalar()*m_f0_prop;
      bool rejflag=compute_f0_prior();
      compute_prior();
      return rejflag;
    };


    inline void accept_f0(){
      m_f0_acc++;
    }

    inline void reject_f0(){
      m_f0=m_f0_old;
      m_f0_prior=m_f0_old_prior;
      m_prior_en=m_old_prior_en;
      m_f0_rej++;
    }


    //Adapt standard deviation of proposal distributions during MCMC execution
    //to avoid over-rejection/over-acceptance of MCMC samples
    inline void update_proposals(){
      m_d_prop*=sqrt(float(m_d_acc+1)/float(m_d_rej+1));
      m_d_prop=min(m_d_prop,maxfloat);
      if (m_includef0){
      	m_f0_prop*=sqrt(float(m_f0_acc+1)/float(m_f0_rej+1));
	m_f0_prop=min(m_f0_prop,maxfloat);
	m_f0_acc=0;
	m_f0_rej=0;
      }
      m_S0_prop*=sqrt(float(m_S0_acc+1)/float(m_S0_rej+1));
      m_S0_prop=min(m_S0_prop,maxfloat);
      m_d_acc=0;
      m_d_rej=0;
      m_S0_acc=0;
      m_S0_rej=0;
      for(unsigned int f=0; f<m_fibres.size();f++){
	m_fibres[f].update_proposals();
      }

      if(!m_fixt1t2){
	m_t1_prop*=sqrt(float(m_t1_acc+1)/float(m_t1_rej+1));
	m_t1_prop=min(m_t1_prop,maxfloat);
	m_t1_acc=0;
	m_t1_rej=0;
	m_t2_prop*=sqrt(float(m_t2_acc+1)/float(m_t2_rej+1));
	m_t2_prop=min(m_t2_prop,maxfloat);
	m_t2_acc=0;
	m_t2_rej=0;
      }

    }


    //Function that performs a single MCMC iteration
    void jump( bool can_use_ard=true ){
      if (m_includef0){     //Try f0
	if(!propose_f0()){
	  if(!reject_f_sum()){
	    compute_prior();
	    compute_likelihood();
	    compute_energy();
	    if(test_energy())
	      accept_f0();
	    else{
	      reject_f0();
	      restore_energy();
	    }
	  }
	  else    //else for rejecting fsum>1
	     reject_f0();
	}
	else     //else for rejecting rejflag returned from propose_f()
	  reject_f0();
      }

      if (!m_fixt1t2){     //Try T1,T2
	if(!propose_t1()){
	  compute_prior();
	  compute_likelihood();
	  compute_energy();
	  if(test_energy()){
	    accept_t1();
	  }
	  else{
	    reject_t1();
	    restore_energy();
	  }
	}
	else{
	  reject_t1();
	}

	if(!propose_t2()){
	  compute_prior();
	  compute_likelihood();
	  compute_energy();
	  if(test_energy()){
	    accept_t2();
	  }
	  else{
	    reject_t2();
	    restore_energy();
	  }
	}
	else
	  reject_t2();


      }



      if(!propose_d()){          //Try d
	compute_prior();
	compute_likelihood();
	compute_energy();
	if(test_energy()){
	  accept_d();
	}
	else{
	  reject_d();
	  restore_energy();
	}
      }
      else
	reject_d();


      if(!propose_S0()){    //Try S0
	compute_prior();
	compute_likelihood();
	compute_energy();
	if(test_energy())
	  accept_S0();
	else{
	  reject_S0();
	  restore_energy();
	}
      }
      else
	reject_S0();

      for(unsigned int f=0;f<m_fibres.size();f++){  //For each fibre
	if(!m_fibres[f].propose_th()){   //Try theta
	  compute_prior();
	  compute_likelihood();
	  compute_energy();

	  if(test_energy())
	    m_fibres[f].accept_th();
	  else{
	    m_fibres[f].reject_th();
	    restore_energy();
	  }
	}
	else
	  m_fibres[f].reject_th();


	if(!m_fibres[f].propose_ph()){  //Try phi
	    compute_prior();
	    compute_likelihood();
	    compute_energy();
	    if(test_energy())
	      m_fibres[f].accept_ph();
	    else{
	      m_fibres[f].reject_ph();
	      restore_energy();
	    }
	  }
	else
	  m_fibres[f].reject_ph();


	if(!m_fibres[f].propose_f( can_use_ard )){  //Try f
	    if(!reject_f_sum()){
	      compute_prior();
	      compute_likelihood();
	      compute_energy();
	      if(test_energy())
		m_fibres[f].accept_f();
	      else{
		m_fibres[f].reject_f();
		restore_energy();
	      }
	    }
	    else   //else for rejectin fsum>1
	      m_fibres[f].reject_f();
	}
	else    //else for rejecting rejflag returned from propose_f()
	  m_fibres[f].reject_f();



      }
    }


    Multifibre& operator=(const Multifibre& rhs){
      m_fibres=rhs.m_fibres;
      m_d=rhs.m_d;
      m_d_old=rhs.m_d_old;
      m_d_prop=rhs.m_d_prop;
      m_d_prior=rhs.m_d_prior;
      m_d_old_prior=rhs.m_d_old_prior;
      m_d_acc=rhs.m_d_acc;
      m_d_rej=rhs.m_d_rej;
      m_S0=rhs.m_S0;
      m_S0_old=rhs.m_S0_old;
      m_S0_prop=rhs.m_S0_prop;
      m_S0_prior=rhs.m_S0_prior;
      m_S0_old_prior=rhs.m_S0_old_prior;
      m_S0_acc=rhs.m_S0_acc;
      m_S0_rej=rhs.m_S0_rej;
      m_f0=rhs.m_f0;
      m_f0_old=rhs.m_f0_old;
      m_f0_prop=rhs.m_f0_prop;
      m_f0_prior=rhs.m_f0_prior;
      m_f0_old_prior=rhs.m_f0_old_prior;
      m_f0_acc=rhs.m_f0_acc;
      m_f0_rej=rhs.m_f0_rej;
      m_prior_en=rhs.m_prior_en;
      m_old_prior_en=rhs.m_old_prior_en;
      m_likelihood_en=rhs.m_likelihood_en;
      m_old_likelihood_en=rhs.m_old_likelihood_en;
      m_energy=rhs.m_energy;
      m_old_energy=rhs.m_old_energy;
      m_ardfudge=rhs.m_ardfudge;
      m_iso_Signal=rhs.m_iso_Signal;
      m_iso_Signal_old=rhs.m_iso_Signal_old;
      //ssfp
      m_fixt1t2=rhs.m_fixt1t2;
      m_T1=rhs.m_T1;
      m_T2=rhs.m_T2;
      m_E1=rhs.m_E1;
      m_E2=rhs.m_E2;
      m_gGG=rhs.m_gGG;
      m_cfm=rhs.m_cfm;
      m_sfm=rhs.m_sfm;

      m_pm_T1=rhs.m_pm_T1;
      m_pv_T1=rhs.m_pv_T1;
      m_min_T1=rhs.m_min_T1;
      m_max_T1=rhs.m_max_T1;

      m_pm_T2=rhs.m_pm_T2;
      m_pv_T2=rhs.m_pv_T2;
      m_min_T2=rhs.m_min_T2;
      m_max_T2=rhs.m_max_T2;


      return *this;
      }


  };

}

#endif
