#!/bin/sh

subjdir=$1


${FSLDIR}/bin/fslmerge -z ${subjdir}.dtifit/mean_M0samples  `${FSLDIR}/bin/imglob ${subjdir}.dtifit/diff_slices/data_slice_*/mean_S0samples*`

for m in FA MD L1 L2 L3 V1 V2 V3 MO;do
    ${FSLDIR}/bin/fslmerge -z ${subjdir}.dtifit/dti_${m}  `${FSLDIR}/bin/imglob ${subjdir}.dtifit/diff_slices/data_slice_*/dti_${m}*`
done




echo Removing intermediate files
if [ `imtest ${subjdir}.dtifit/dti_MO` -eq 1 ];then
    rm -rf ${subjdir}.dtifit/diff_slices
    rm -f ${subjdir}/data_slice_*
    rm -f ${subjdir}/nodif_brain_mask_slice_*
    if [ `${FSLDIR}/bin/imtest ${subjdir}/T1map` -eq 1 ];then
	rm -f ${subjdir}/T1map_slice_*
    fi
    if [ `${FSLDIR}/bin/imtest ${subjdir}/T2map` -eq 1 ];then
	rm -f ${subjdir}/T2map_slice_*
    fi
    if [ `${FSLDIR}/bin/imtest ${subjdir}/B1map` -eq 1 ];then
	rm -f ${subjdir}/B1map_slice_*
    fi
fi

echo Done
