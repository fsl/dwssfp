/* Xfibres Diffusion Partial Volume Model

    Saad Jbabdi, Jennifer McNab  - FMRIB Image Analysis Group

    Copyright (C) 2005 University of Oxford  */

/*  CCOPYRIGHT  */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <math.h>
#include "utils/log.h"
#include "utils/tracer_plus.h"
#include "miscmaths/miscprob.h"
#include "miscmaths/miscmaths.h"
#include "miscmaths/nonlin.h"
#include "newimage/newimageall.h"
#include "stdlib.h"
#include "fibre.h"
#include "xfibresoptions.h"
#include "diffmodels.h"

using namespace DTIFIT;
using namespace FIBRE;
using namespace Xfibres;
using namespace Utilities;
using namespace NEWMAT;
using namespace NEWIMAGE;
using namespace MISCMATHS;

#define twopi 6.28318531

////////////////////////////////////////////////
//       Some USEFUL FUNCTIONS
////////////////////////////////////////////////

Matrix form_Amat(const Matrix& r,const Matrix& b)
{
  Matrix A(r.Ncols(),7);
  Matrix tmpvec(3,1), tmpmat;

  for( int i = 1; i <= r.Ncols(); i++){
    tmpvec << r(1,i) << r(2,i) << r(3,i);
    tmpmat = tmpvec*tmpvec.t()*b(1,i);
    A(i,1) = tmpmat(1,1);
    A(i,2) = 2*tmpmat(1,2);
    A(i,3) = 2*tmpmat(1,3);
    A(i,4) = tmpmat(2,2);
    A(i,5) = 2*tmpmat(2,3);
    A(i,6) = tmpmat(3,3);
    A(i,7) = 1;
  }
  return A;
}
void sph2cart(const float& th,const float& ph,ColumnVector& x){
  x.ReSize(3);
  x<<sin(th)*cos(ph)
   <<sin(th)*sin(ph)
   <<cos(th);
}



////////////////////////////////////////////
//       MCMC SAMPLE STORAGE
////////////////////////////////////////////



class Samples{
  xfibresOptions& opts;
  Matrix m_dsamples;
  Matrix m_S0samples;
  Matrix m_f0samples;
  Matrix m_lik_energy;
  Matrix m_t1samples;
  Matrix m_t2samples;

  vector<Matrix> m_thsamples;
  vector<Matrix> m_phsamples;
  vector<Matrix> m_fsamples;

  //for storing means
  RowVector m_mean_dsamples;
  RowVector m_mean_S0samples;
  RowVector m_mean_f0samples;
  RowVector m_mean_t1samples;
  RowVector m_mean_t2samples;
  vector<Matrix> m_dyadic_vectors;
  vector<RowVector> m_mean_fsamples;

  float m_sum_d;
  float m_sum_S0;
  float m_sum_f0;
  float m_sum_t1;
  float m_sum_t2;
  vector<SymmetricMatrix> m_dyad;
  vector<float> m_sum_f;
  int m_nsamps;
  ColumnVector m_vec;

  Matrix      m_matrix2volkey;

public:

  Samples(Matrix matrix2volkey):
    opts(xfibresOptions::getInstance()),m_matrix2volkey(matrix2volkey){

    int count=0;
    int nsamples=0;
    int nvoxels=matrix2volkey.Nrows();

    for(int i=0;i<opts.njumps.value();i++){
      count++;
      if(count==opts.sampleevery.value()){
	count=0;nsamples++;
      }
    }


    m_dsamples.ReSize(nsamples,nvoxels);
    m_dsamples=0;
    m_S0samples.ReSize(nsamples,nvoxels);
    m_S0samples=0;
    m_lik_energy.ReSize(nsamples,nvoxels);

    m_mean_dsamples.ReSize(nvoxels);
    m_mean_dsamples=0;
    m_mean_S0samples.ReSize(nvoxels);
    m_mean_S0samples=0;


    if(!opts.fixt1t2.value()){
      m_t1samples.ReSize(nsamples,nvoxels);
      m_t1samples=0;
      m_t2samples.ReSize(nsamples,nvoxels);
      m_t2samples=0;
      m_mean_t1samples.ReSize(nvoxels);
      m_mean_t1samples=0;
      m_mean_t2samples.ReSize(nvoxels);
      m_mean_t2samples=0;
      m_sum_t1=0;
      m_sum_t2=0;
    }


    Matrix tmpvecs(3,nvoxels);
    tmpvecs=0;
    m_sum_d=0;
    m_sum_S0=0;

    if (opts.f0.value()){
      m_f0samples.ReSize(nsamples,nvoxels);
      m_f0samples=0;
      m_mean_f0samples.ReSize(nvoxels);
      m_mean_f0samples=0;
      m_sum_f0=0;
    }


    SymmetricMatrix tmpdyad(3);
    tmpdyad=0;
    m_nsamps=nsamples;
    m_vec.ReSize(3);
    for(int f=0;f<opts.nfibres.value();f++){
      m_thsamples.push_back(m_S0samples);
      m_phsamples.push_back(m_S0samples);
      m_fsamples.push_back(m_S0samples);


      m_dyadic_vectors.push_back(tmpvecs);
      m_mean_fsamples.push_back(m_mean_S0samples);

      m_sum_f.push_back(0);
      m_dyad.push_back(tmpdyad);
    }

  }


  void record(Multifibre& mfib, int vox, int samp){
    m_dsamples(samp,vox)=mfib.get_d();
    m_sum_d+=mfib.get_d();
    if (opts.f0.value()){
      m_f0samples(samp,vox)=mfib.get_f0();
      m_sum_f0+=mfib.get_f0();
    }
    if(!opts.fixt1t2.value()){
      m_t1samples(samp,vox)=mfib.get_t1();
      m_sum_t1+=mfib.get_t1();
      m_t2samples(samp,vox)=mfib.get_t2();
      m_sum_t2+=mfib.get_t2();
    }


    m_S0samples(samp,vox)=mfib.get_S0();
    m_sum_S0+=mfib.get_S0();
    m_lik_energy(samp,vox)=mfib.get_likelihood_energy();
    for(int f=0;f<opts.nfibres.value();f++){
      float th=mfib.fibres()[f].get_th();
      float ph=mfib.fibres()[f].get_ph();
      m_thsamples[f](samp,vox)=th;
      m_phsamples[f](samp,vox)=ph;
      m_fsamples[f](samp,vox)=mfib.fibres()[f].get_f();
      //for means
      m_vec << sin(th)*cos(ph) << sin(th)*sin(ph)<<cos(th) ;
      m_dyad[f] << m_dyad[f]+m_vec*m_vec.t();
      m_sum_f[f]+=mfib.fibres()[f].get_f();
    }
  }

  void finish_voxel(int vox){
    m_mean_dsamples(vox)=m_sum_d/m_nsamps;
    if(opts.f0.value())
      m_mean_f0samples(vox)=m_sum_f0/m_nsamps;

    m_mean_S0samples(vox)=m_sum_S0/m_nsamps;
    m_sum_d=0;
    m_sum_S0=0;
    if (opts.f0.value())
      m_sum_f0=0;

    if(!opts.fixt1t2.value()){
      m_mean_t1samples(vox)=m_sum_t1/m_nsamps;
      m_sum_t1=0;
      m_mean_t2samples(vox)=m_sum_t2/m_nsamps;
      m_sum_t2=0;
    }

    DiagonalMatrix dyad_D; //eigenvalues
    Matrix dyad_V; //eigenvectors
    int nfibs=0;
    for(int f=0;f<opts.nfibres.value();f++){
      EigenValues(m_dyad[f],dyad_D,dyad_V);
      int maxeig;
      if(dyad_D(1)>dyad_D(2)){
	if(dyad_D(1)>dyad_D(3)) maxeig=1;
	else maxeig=3;
      }
      else{
	if(dyad_D(2)>dyad_D(3)) maxeig=2;
	else maxeig=3;
      }
      m_dyadic_vectors[f](1,vox)=dyad_V(1,maxeig);
      m_dyadic_vectors[f](2,vox)=dyad_V(2,maxeig);
      m_dyadic_vectors[f](3,vox)=dyad_V(3,maxeig);

      if((m_sum_f[f]/m_nsamps)>0.05){
	nfibs++;
      }
      m_mean_fsamples[f](vox)=m_sum_f[f]/m_nsamps;

      m_dyad[f]=0;
      m_sum_f[f]=0;
    }

  }



  void save(const volume<float>& mask){
    volume4D<float> tmp;
    //So that I can sort the output fibres into
    // files ordered by fibre fractional volume..
    vector<Matrix> thsamples_out=m_thsamples;
    vector<Matrix> phsamples_out=m_phsamples;
    vector<Matrix> fsamples_out=m_fsamples;

    vector<Matrix> dyadic_vectors_out=m_dyadic_vectors;
    vector<Matrix> mean_fsamples_out;
    for(unsigned int f=0;f<m_mean_fsamples.size();f++)
      mean_fsamples_out.push_back(m_mean_fsamples[f]);

    Log& logger = LogSingleton::getInstance();

    tmp.setmatrix(m_mean_dsamples,mask);
    tmp.setDisplayMaximumMinimum(tmp.max(),0);
    save_volume4D(tmp,logger.appendDir("mean_dsamples"));

    if (opts.f0.value()){
      tmp.setmatrix(m_mean_f0samples,mask);
      tmp.setDisplayMaximumMinimum(1,0);
      save_volume4D(tmp,logger.appendDir("mean_f0samples"));
      tmp.setmatrix(m_f0samples,mask);
      tmp.setDisplayMaximumMinimum(1,0);
      save_volume4D(tmp,logger.appendDir("f0samples"));
    }

    if (!opts.fixt1t2.value()){
      // remember to turn back to milliseconds
      tmp.setmatrix(m_mean_t1samples*1000,mask);
      tmp.setDisplayMaximumMinimum(tmp.max(),0);
      save_volume4D(tmp,logger.appendDir("mean_t1samples"));
      tmp.setmatrix(m_t1samples*1000,mask);
      tmp.setDisplayMaximumMinimum(tmp.max(),0);
      save_volume4D(tmp,logger.appendDir("t1samples"));

      tmp.setmatrix(m_mean_t2samples*1000,mask);
      tmp.setDisplayMaximumMinimum(tmp.max(),0);
      save_volume4D(tmp,logger.appendDir("mean_t2samples"));
      tmp.setmatrix(m_t2samples*1000,mask);
      tmp.setDisplayMaximumMinimum(tmp.max(),0);
      save_volume4D(tmp,logger.appendDir("t2samples"));
    }


    tmp.setmatrix(m_mean_S0samples,mask);
    tmp.setDisplayMaximumMinimum(tmp.max(),0);
    save_volume4D(tmp,logger.appendDir("mean_S0samples"));

    //Sort the output based on mean_fsamples
    //
    vector<Matrix> sumf;
    for(int f=0;f<opts.nfibres.value();f++){
      Matrix tmp=sum(m_fsamples[f],1);
      sumf.push_back(tmp);
    }
    for(int vox=1;vox<=m_dsamples.Ncols();vox++){
      vector<pair<float,int> > sfs;
      pair<float,int> ftmp;

      for(int f=0;f<opts.nfibres.value();f++){
	ftmp.first=sumf[f](1,vox);
	ftmp.second=f;
	sfs.push_back(ftmp);
      }
      sort(sfs.begin(),sfs.end());

      float th,ph;ColumnVector x(3);
      for(int samp=1;samp<=m_dsamples.Nrows();samp++){
	for(int f=0;f<opts.nfibres.value();f++){;
	  th=m_thsamples[sfs[(sfs.size()-1)-f].second](samp,vox);
	  ph=m_phsamples[sfs[(sfs.size()-1)-f].second](samp,vox);

	  thsamples_out[f](samp,vox)=th;//std::fmod(th,(float)m_PI);
	  phsamples_out[f](samp,vox)=ph;//std::fmod(ph,(float)twopi);
	  fsamples_out[f](samp,vox)=m_fsamples[sfs[(sfs.size()-1)-f].second](samp,vox);
	}
      }

      for(int f=0;f<opts.nfibres.value();f++){
	mean_fsamples_out[f](1,vox)=m_mean_fsamples[sfs[(sfs.size()-1)-f].second](vox);
	dyadic_vectors_out[f](1,vox)=m_dyadic_vectors[sfs[(sfs.size()-1)-f].second](1,vox);
	dyadic_vectors_out[f](2,vox)=m_dyadic_vectors[sfs[(sfs.size()-1)-f].second](2,vox);
	dyadic_vectors_out[f](3,vox)=m_dyadic_vectors[sfs[(sfs.size()-1)-f].second](3,vox);
      }

    }
    // save the sorted fibres
    for(int f=0;f<opts.nfibres.value();f++){
      tmp.setmatrix(thsamples_out[f],mask);
      tmp.setDisplayMaximumMinimum(tmp.max(),tmp.min());
      string oname="th"+num2str(f+1)+"samples";
      save_volume4D(tmp,logger.appendDir(oname));

      tmp.setmatrix(phsamples_out[f],mask);
      tmp.setDisplayMaximumMinimum(tmp.max(),tmp.min());
      oname="ph"+num2str(f+1)+"samples";
      save_volume4D(tmp,logger.appendDir(oname));

      tmp.setmatrix(fsamples_out[f],mask);
      tmp.setDisplayMaximumMinimum(1,0);
      oname="f"+num2str(f+1)+"samples";
      save_volume4D(tmp,logger.appendDir(oname));

      tmp.setmatrix(mean_fsamples_out[f],mask);
      tmp.setDisplayMaximumMinimum(1,0);
      oname="mean_f"+num2str(f+1)+"samples";
      save_volume(tmp[0],logger.appendDir(oname));

      tmp.setmatrix(dyadic_vectors_out[f],mask);
      tmp.setDisplayMaximumMinimum(1,-1);
      oname="dyads"+num2str(f+1);
      save_volume4D(tmp,logger.appendDir(oname));
    }
  }

};


////////////////////////////////////////////
//       MCMC HANDLING
////////////////////////////////////////////



class xfibresVoxelManager{

  xfibresOptions& opts;

  Samples& m_samples;
  int m_voxelnumber;
  const ColumnVector m_data;
  const ColumnVector& m_alpha;
  const ColumnVector& m_beta;
  const Matrix& m_bvecs;
  const Matrix& m_bvals;

  const Matrix& m_diffGradDur;
  const Matrix& m_diffGradAmp;
  const Matrix& m_TR;
  const Matrix& m_flipAngle;

  Multifibre m_multifibre;
public:
  xfibresVoxelManager(const ColumnVector& data,const ColumnVector& alpha,
		      const ColumnVector& beta, const Matrix& r,const Matrix& b,
		      const Matrix& _diffGradDur,const Matrix& _diffGradAmp,const Matrix& _TR,const Matrix& _flipAngle,
		      Samples& samples,int voxelnumber):
    opts(xfibresOptions::getInstance()),
    m_samples(samples),m_voxelnumber(voxelnumber),m_data(data),
    m_alpha(alpha), m_beta(beta), m_bvecs(r), m_bvals(b),
    m_diffGradDur(_diffGradDur),m_diffGradAmp(_diffGradAmp),m_TR(_TR),m_flipAngle(_flipAngle),
    m_multifibre(m_data,m_alpha,m_beta,m_bvals,opts.nfibres.value(),
		 m_diffGradDur,m_diffGradAmp,m_TR,m_flipAngle,opts.fixt1t2.value(),
		 opts.fudge.value(),opts.f0.value(),opts.ardf0.value()){}


    void initialise(const Matrix& Amat,float t1,float t2,float b1){

      initialise_nonlin(t1,t2,b1);

      m_multifibre.initialise_energies();
      m_multifibre.initialise_props();
    }



    void initialise_nonlin(float t1,float t2,float b1){

      float pvmS0, pvmd, pvmf0=0;
      ColumnVector pvmf,pvmth,pvmph;

      PVM_single_c pvm(m_data,m_bvecs,m_bvals,opts.nfibres.value(),opts.f0.value());
      //PVM_single pvm(m_data,m_bvecs,m_bvals,opts.nfibres.value(),opts.f0.value());
      pvm.fit(); // this will give th,ph,f in the correct order
      //pvm.print();

      //DTI dti(m_data,m_bvecs,m_bvals);
      //dti.linfit();


      pvmf  = pvm.get_f();
      pvmth = pvm.get_th();
      pvmph = pvm.get_ph();
      pvmS0 = pvm.get_s0();
      // turn into M0
      float te=28.0/1e3;
      //if(opts.te.set())
      //te=opts.te.value();
      float tr=42.0/1e3;
      //if(opts.tr.set())
      //tr=opts.tr.value();
      pvmS0 = pvmS0/(std::exp(-te/t2)*(1.0-std::exp(-tr/t1)));
      //OUT(pvmS0);

      pvmd  = pvm.get_d();

    if (opts.f0.value()){
      pvmf0=pvm.get_f0();
      m_multifibre.set_f0(pvmf0);
    }

    if(pvmd<0)
      pvmd=2e-3;


    // set manually for testing
    // pvmth(1)=0;
//     pvmph(1)=0;
//     pvmf(1)=0.5;
//     pvmd=2e-4;
//     pvmS0=10000;
//     t1=800.0/1000.0;
//     t2=50.0/1000.0;
//     OUT(b1);


    m_multifibre.set_S0(pvmS0);
    m_multifibre.set_d(pvmd);

    // init T1,T2
    m_multifibre.set_t1t2b1(t1,t2,b1);
    // set priors here?

    if(opts.nfibres.value()>0){
      m_multifibre.addfibre(pvmth(1),
			    pvmph(1),
			    pvmf(1),
			    opts.all_ard.value());//if all_ard, then turn ard on here (SJ)
      for(int i=2; i<=opts.nfibres.value();i++){
	m_multifibre.addfibre(pvmth(i),
			      pvmph(i),
			      pvmf(i),
			      !opts.no_ard.value());
      }
    }
    //

    //OUT(m_multifibre.get_prediction().t());
    //exit(1);

  }


  void runmcmc(){
    int count=0, recordcount=0,sample=1;//sample will index a newmat matrix
    for( int i =0;i<opts.nburn.value();i++){
      m_multifibre.jump( !opts.no_ard.value() );

      count++;
      if(count==opts.updateproposalevery.value()){
	m_multifibre.update_proposals();
	count=0;
      }
    }

    for( int i =0;i<opts.njumps.value();i++){
      m_multifibre.jump(!opts.no_ard.value());
      count++;

      if(opts.verbose.value())
	{
	  cout<<endl<<i<<" "<<endl<<endl;
	  //m_multifibre.report();

	}
      recordcount++;
      if(recordcount==opts.sampleevery.value()){
	m_samples.record(m_multifibre,m_voxelnumber,sample);
	sample++;
	recordcount=0;
      }
      if(count==opts.updateproposalevery.value()){
	m_multifibre.update_proposals();
	count=0;

      }
    }

    //OUT(m_multifibre.get_prediction().t());
    //OUT(m_multifibre.get_t1());
    //OUT(m_multifibre.get_t2());
    //m_multifibre.report();

    m_samples.finish_voxel(m_voxelnumber);
  }

};




////////////////////////////////////////////
//       MAIN
////////////////////////////////////////////

int main(int argc, char *argv[])
{

    // Setup logging:
    Log& logger = LogSingleton::getInstance();
    xfibresOptions& opts = xfibresOptions::getInstance();
    opts.parse_command_line(argc,argv,logger);
    srand(opts.seed.value());
    Matrix bvals,bvecs,matrix2volkey;
    volume<float> mask;
    volume<int> vol2matrixkey;
    bvals=read_ascii_matrix(opts.bvalsfile.value());
    bvecs=read_ascii_matrix(opts.bvecsfile.value());
    if(bvecs.Nrows()>3) bvecs=bvecs.t();
    if(bvals.Nrows()>1) bvals=bvals.t();
    for(int i=1;i<=bvecs.Ncols();i++){
      float tmpsum=sqrt(bvecs(1,i)*bvecs(1,i)+bvecs(2,i)*bvecs(2,i)+bvecs(3,i)*bvecs(3,i));
      if(tmpsum!=0){
	bvecs(1,i)=bvecs(1,i)/tmpsum;
	bvecs(2,i)=bvecs(2,i)/tmpsum;
	bvecs(3,i)=bvecs(3,i)/tmpsum;
      }
    }

    ColumnVector datam;
    volume4D<float> data;
    read_volume4D(data,opts.datafile.value());
    read_volume(mask,opts.maskfile.value());
    //datam=data.matrix(mask);
    matrix2volkey=data.matrix2volkey(mask);
    vol2matrixkey=data.vol2matrixkey(mask);


    // SSFP
    Matrix diffGradAmp,diffGradDur,TR,flipAngle;
    diffGradAmp=read_ascii_matrix(opts.gradampfile.value());
    diffGradDur=read_ascii_matrix(opts.graddurfile.value());
    TR=read_ascii_matrix(opts.trfile.value());
    flipAngle=read_ascii_matrix(opts.flipanglefile.value());
    flipAngle = flipAngle*M_PI/180.0;


    volume<float> T1map,T2map,B1map;
    if(opts.t1file.value()!=""){
      read_volume(T1map,opts.t1file.value());
    }
    if(opts.t2file.value()!=""){
      read_volume(T2map,opts.t2file.value());
    }
    if(opts.b1file.value()!=""){
      read_volume(B1map,opts.b1file.value());
    }

    Matrix Amat;
    ColumnVector alpha, beta;
    Amat=form_Amat(bvecs,bvals);
    cart2sph(bvecs,alpha,beta);
    Samples samples(matrix2volkey);

    float t1,t2,b1;
    t1=500;
    t2=50;
    b1=1;

    //for(int vox=1;vox<=datam.Ncols();vox++){

    int vox=0;
    datam.ReSize(data.tsize());
    for(int z=0;z<mask.zsize();z++){
      for(int y=0;y<mask.ysize();y++){
	for(int x=0;x<mask.xsize();x++){
	  if(mask(x,y,z)==0)continue;
	  for(int t=0;t<data.tsize();t++)
	    datam(t+1)=data(x,y,z,t);
	  vox++;
	  cout<<x<<" "<<y<<" "<<z<<endl;
	  //cout <<vox<<"/"<<datam.Ncols()<<endl;
	  xfibresVoxelManager  vm(datam,alpha,beta,bvecs,bvals,
				  diffGradDur,diffGradAmp,TR,flipAngle,
				  samples,vox);

	  //OUT(datam.t());

	  if(opts.t1file.value()!="")
	    t1=T1map(x,y,z);
	  if(opts.t2file.value()!="")
	    t2=T2map(x,y,z);
	  if(opts.t2file.value()!="")
	    b1=B1map(x,y,z);

	  if(t1==0)t1=500;
	  if(t2==0)t2=50;
	  //OUT(t1);OUT(t2);OUT(b1);
	  vm.initialise(Amat,t1/1000.0,t2/1000.0,b1);
	  vm.runmcmc();

	}
      }
    }


//}

    samples.save(mask);


  return 0;
}
