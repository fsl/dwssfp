#!/bin/sh

subjdir=$1

echo Copying files to bedpost directory
cp ${subjdir}/bvecs ${subjdir}/bvals ${subjdir}.dtifit
${FSLDIR}/bin/imcp ${subjdir}/nodif_brain_mask ${subjdir}.dtifit

if [ `${FSLDIR}/bin/imtest ${subjdir}/nodif` = 1 ] ; then
    ${FSLDIR}/bin/fslmaths ${subjdir}/nodif -mas ${subjdir}/nodif_brain_mask ${subjdir}.dtifit/nodif_brain
fi

${FSLDIR}/bin/fslslice ${subjdir}/data
${FSLDIR}/bin/fslslice ${subjdir}/nodif_brain_mask


if [ `${FSLDIR}/bin/imtest ${subjdir}/T1map` -eq 1 ];then
    ${FSLDIR}/bin/fslslice ${subjdir}/T1map
fi
if [ `${FSLDIR}/bin/imtest ${subjdir}/T2map` -eq 1 ];then
    ${FSLDIR}/bin/fslslice ${subjdir}/T2map
fi
if [ `${FSLDIR}/bin/imtest ${subjdir}/B1map` -eq 1 ];then
    ${FSLDIR}/bin/fslslice ${subjdir}/B1map
fi

echo Done
