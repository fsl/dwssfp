/*  Copyright (C) 2012 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef __DTI_H_
#define __DTI_H_


#include <iostream>
#include "stdlib.h"
#include <cmath>
#include "cprob/libprob.h"
#include "armawrap/newmat.h"
#include "miscmaths/miscprob.h"



namespace DTIFIT {

const float maxfloat=1e10;
const float minfloat=1e-10;
const float maxlogfloat=23;
const float minlogfloat=-23;

  NEWMAT::ReturnMatrix Cross(const NEWMAT::ColumnVector& A,const NEWMAT::ColumnVector& B){
    NEWMAT::ColumnVector res(3);
    res << A(2)*B(3)-A(3)*B(2)
	<< A(3)*B(1)-A(1)*B(3)
	<< A(1)*B(2)-B(1)*A(2);
    res.Release();
    return res;
  }
  NEWMAT::ReturnMatrix Genrot(const NEWMAT::ColumnVector& V,const float& ps){
    NEWMAT::Matrix R(3,3);
    float C,S,x,y,z;
    C=std::cos(ps);S=std::sin(ps);
    x=V(1);y=V(2);z=V(3);

    R << x*x+C*(1-x*x) << x*y*(1-C)-z*S << z*x*(1-C)+y*S
      << x*y*(1-C)+z*S << y*y+C*(1-y*y) << y*z*(1-C)-x*S
      << z*x*(1-C)-y*S << y*z*(1-C)+x*S << z*z+C*(1-z*z);

    R.Release();
    return R;

  }
  void tpp_to_vecs(const std::vector<float>& tpp,NEWMAT::Matrix& V){
    NEWMAT::ColumnVector V1(3),V2(3),V3(3);
    V1 << std::sin(tpp[0]) * std::cos(tpp[1])
       << std::sin(tpp[0]) * std::sin(tpp[1])
       << std::cos(tpp[0]);
    NEWMAT::Matrix R;
    R=Genrot(V1,tpp[2]);
    NEWMAT::ColumnVector Nxy(3);
    Nxy << V1(2) << -V1(1) << 0.0;
    if(Nxy.SumAbsoluteValue()>0)
      Nxy=Nxy/std::sqrt(Nxy.SumSquare());
    else
      Nxy<<1<<0<<0;
    V2=R*Nxy;
    V3=Cross(V1,V2);
    V.ReSize(3,3);
    V << V1(1) << V2(1) << V3(1)
      << V1(2) << V2(2) << V3(2)
      << V1(3) << V2(3) << V3(3);
  }
  void vecs_to_tpp(const std::vector<NEWMAT::ColumnVector>& vecs,std::vector<float>& tpp){
    tpp.resize(3);
    float th,ph,ps;
    MISCMATHS::cart2sph(vecs[0],th,ph);   //initialise th and ph
    tpp[0]=th;tpp[1]=ph;
    NEWMAT::ColumnVector Nxy(3);NEWMAT::Matrix Rpos,Rneg;
    Nxy<<vecs[0](2)<<-vecs[0](1)<<0.0;
    Nxy=Nxy/sqrt(Nxy.SumSquare());
    float pstmp=(SP(Nxy,vecs[1])).Sum();
    ps=std::acos(pstmp);
    Rpos=Genrot(vecs[0],ps);
    Rneg=Genrot(vecs[0],-ps);
    if((SP(vecs[1],Rneg*Nxy)).Sum()>(SP(vecs[1],Rpos*Nxy)).Sum()){
      ps=-ps;
    }
    tpp[2]=ps;
  }




 /////////////////////////////////////////////////////////////////////////////////////////////
 ////Class that represents one anisotropic compartment of the PVM model in an MCMC framework
 ////////////////////////////////////////////////////////////////////////////////////////////
  class SSFP_DTI{
    std::vector<float> m_l;
    std::vector<float> m_tpp;
    float m_S0;

    std::vector<float> m_l_prop;
    std::vector<float> m_tpp_prop;
    float m_S0_prop;

    std::vector<float> m_l_old;
    std::vector<float> m_tpp_old;
    float m_S0_old;

    std::vector<float> m_l_prior;
    std::vector<float> m_tpp_prior;
    float m_S0_prior;

    std::vector<float> m_l_old_prior;
    std::vector<float> m_tpp_old_prior;
    float m_S0_old_prior;

    std::vector<int> m_l_acc;
    std::vector<int> m_l_rej;
    std::vector<int> m_tpp_acc;
    std::vector<int> m_tpp_rej;
    int m_S0_acc;
    int m_S0_rej;

    NEWMAT::ColumnVector m_Signal;
    NEWMAT::ColumnVector m_Signal_old;

    NEWMAT::Matrix m_V,m_V_old;


    float m_prior_en;
    float m_old_prior_en;
    float m_likelihood_en;
    float m_old_likelihood_en;
    float m_energy;
    float m_old_energy;

    const NEWMAT::ColumnVector& m_data;
    const NEWMAT::Matrix& m_bvecs;
    const NEWMAT::Matrix& m_bvals;

    // ssfp params
    const float& m_T1;
    const float& m_T2;
    NEWMAT::ColumnVector m_E1,m_E2;
    NEWMAT::ColumnVector m_gGG;
    const NEWMAT::Matrix& diffGradDur;
    const NEWMAT::Matrix& diffGradAmp;
    const NEWMAT::Matrix& TR;
    const NEWMAT::Matrix& flipAngle;
    float m_B1;

    double A1,A2;
    double K,F1;
    double A2_03;
    double Mminus;
    double b1, b2;
    double s,r_bux;
    double K_top,K_bottom;
    double Mminus_top,Mminus_bottom;
    double flip_mod;

    NEWMAT::ColumnVector m_cfm,m_sfm;


 public:
    //constructors::

    SSFP_DTI( const NEWMAT::ColumnVector& data,const NEWMAT::Matrix& bvecs,const NEWMAT::Matrix& bvals,
	      const NEWMAT::Matrix& _diffGradDur,const NEWMAT::Matrix& _diffGradAmp,
	      const NEWMAT::Matrix& _TR,const NEWMAT::Matrix& _flipAngle,
	      const float& t1,const float& t2,float b1):
      m_data(data),m_bvecs(bvecs),m_bvals(bvals),
      m_T1(t1),m_T2(t2),diffGradDur(_diffGradDur),diffGradAmp(_diffGradAmp),TR(_TR),flipAngle(_flipAngle)
   {
      m_B1=b1;
      init_ssfp();

      m_l.resize(3,0.002);
      m_l_old.resize(3,0.002);
      m_l_prop.resize(3,0.001);
      m_l_prior.resize(3,0);
      m_l_old_prior.resize(3,0);
      compute_l_prior();

      m_tpp.resize(3,0);
      m_tpp_old.resize(3,0);
      m_tpp_prop.resize(3,0.2);
      m_tpp_prior.resize(3,0);
      m_tpp_old_prior.resize(3,0);
      compute_tpp_prior();

      tpp_to_vecs(m_tpp,m_V);
      m_V_old=m_V;
      m_V.ReSize(3,3);

      m_S0=1;
      m_S0_old=m_S0;
      m_S0_prop=0.2;
      m_S0_prior=0;
      compute_S0_prior();

      m_Signal.ReSize(bvals.Ncols());
      m_Signal=0;
      m_Signal_old=m_Signal;

      compute_prior();
      compute_signal();

      m_l_acc.resize(3,0);
      m_l_rej.resize(3,0);
      m_tpp_acc.resize(3,0);
      m_tpp_rej.resize(3,0);
      m_S0_acc=0;
      m_S0_rej=0;


    }


    ~SSFP_DTI(){}

    SSFP_DTI(const SSFP_DTI& rhs) :
      m_data(rhs.m_data),m_bvecs(rhs.m_bvecs),m_bvals(rhs.m_bvals),
      m_T1(rhs.m_T1),m_T2(rhs.m_T2),diffGradDur(rhs.diffGradDur),diffGradAmp(rhs.diffGradAmp),
      TR(rhs.TR),flipAngle(rhs.flipAngle)
    {
      *this=rhs;
    }


   void init_ssfp(){
     float gyro=2.0*M_PI*4258.0;
     m_cfm.ReSize(m_bvals.Ncols());
     m_sfm.ReSize(m_bvals.Ncols());
     m_E1.ReSize(m_bvals.Ncols());
     m_E2.ReSize(m_bvals.Ncols());
     m_gGG.ReSize(m_bvals.Ncols());
     float flip_mod;
     for(int i=1;i<=m_bvals.Ncols();i++){
       //Use measured B1map to modify prescribed flip angle as required
       if (m_B1!=0)
	 {flip_mod=flipAngle(1,i)*m_B1;}
       else
	 {flip_mod=flipAngle(1,i);}
       m_cfm(i)=cos(flip_mod);
       m_sfm(i)=sin(flip_mod);
       m_gGG(i)=gyro*diffGradAmp(1,i)*diffGradDur(1,i)/10.0;
       m_E1(i)=exp(-TR(1,i)/m_T1);
       m_E2(i)=exp(-TR(1,i)/m_T2);
     }
   }
    void initialise_dti(const std::vector<float>& l,const std::vector<float> tpp,const float& s0){
      m_l=l;m_l_old=m_l;
      m_tpp=tpp;m_tpp_old=m_tpp;
      m_S0=s0;m_S0_old=m_S0;
    }

    void initialise_energies(){
      compute_l_prior();
      compute_tpp_prior();
      compute_S0_prior();

      m_prior_en=0;
      compute_prior();
      m_likelihood_en=0;
      compute_signal();
      compute_likelihood();
      compute_energy();
    }

    float get_S0()const{
      return m_S0;
    }
    NEWMAT::ReturnMatrix get_tensor_as_vec()const{
      NEWMAT::Matrix V(3,3);
      tpp_to_vecs(m_tpp,V);
      NEWMAT::SymmetricMatrix Tens(3);
      Tens << m_l[0]*V.Column(1)*V.Column(1).t() +
	m_l[1]*V.Column(2)*V.Column(2).t() +
	m_l[2]*V.Column(3)*V.Column(3).t();
      NEWMAT::ColumnVector ret(6);
      ret << Tens(1,1)<<Tens(2,1)<<Tens(3,1)<<Tens(2,2)<<Tens(3,2)<<Tens(3,3);
      ret.Release();
      return ret;
    }


    //Initialize standard deviations for the proposal distributions
    void initialise_props(){
      m_l_prop.resize(3,0.001);
      m_tpp_prop.resize(3,.2);
      m_S0_prop=m_S0/10.0;
    }
    NEWMAT::ColumnVector get_prediction()const{return (m_S0*m_Signal);}

    void restoreSignal() {
      m_Signal=m_Signal_old;
    }

    float get_prior() const{ return m_prior_en;}

   float compute_buxton(const int&i){
     float adc=0,tmp;
     tmp=SP(m_bvecs.Column(i),m_V.Column(1)).Sum();
     adc += m_l[0]*tmp*tmp;
     tmp=SP(m_bvecs.Column(i),m_V.Column(2)).Sum();
     adc += m_l[1]*tmp*tmp;
     tmp=SP(m_bvecs.Column(i),m_V.Column(3)).Sum();
     adc += m_l[2]*tmp*tmp;


     double E1=m_E1(i);
     double E2=m_E2(i);


     b1=m_gGG(i);
     b2=b1;
     b1 *= adc*b1*TR(1,i);
     b2 *= adc*b2*diffGradDur(1,i);

     A1=std::exp(-b1);
     A2=std::exp(-b2);

     A2_03=exp(-b2/3.0);

     s=E2*A1/A2_03/A2_03/A2_03/A2_03*(1-E1*m_cfm(i))+E2/A2_03*(m_cfm(i)-1);
     r_bux=1 - E1*m_cfm(i)+E2*E2*A1*A2_03*(m_cfm(i)-E1);
     K_top=1-E1*A1*m_cfm(i)-E2*E2*A1*A1/A2_03/A2_03*(E1*A1-m_cfm(i));
     K_bottom=(E2*A1/A2_03/A2_03/A2_03/A2_03)*(1+m_cfm(i))*(1-E1*A1);
     K=K_top/K_bottom;
     F1=K - std::sqrt(K*K-A2*A2);

     Mminus_top=-(1-E1)*E2/A2_03/A2_03*(F1-E2*A1*A2_03*A2_03)*m_sfm(i);
     Mminus_bottom=r_bux-F1*s;
     Mminus=fabs(Mminus_top/Mminus_bottom);

     return(Mminus);

   }
    float compute_spinecho(const int& i){
      float adc=0,tmp;
      tmp=SP(m_bvecs.Column(i),m_V.Column(1)).Sum();
      adc += m_l[0]*tmp*tmp;
      tmp=SP(m_bvecs.Column(i),m_V.Column(2)).Sum();
      adc += m_l[1]*tmp*tmp;
      tmp=SP(m_bvecs.Column(i),m_V.Column(3)).Sum();
      adc += m_l[2]*tmp*tmp;

      return( std::exp(-m_bvals(1,i)*adc) );
    }


    //Adapt the standard deviation of the proposal distributions during MCMC execution
    //to avoid over-rejection/over-acceptance of samples
    inline void update_proposals(){
      for(unsigned int i=0;i<3;i++){
        m_l_prop[i]*=std::sqrt(float(m_l_acc[i]+1)/float(m_l_rej[i]+1));
	m_l_prop[i]=std::min(m_l_prop[i],maxfloat);
	m_tpp_prop[i]*=std::sqrt(float(m_tpp_acc[i]+1)/float(m_tpp_rej[i]+1));
	m_tpp_prop[i]=std::min(m_tpp_prop[i],maxfloat);
	m_l_acc[i]=0;
	m_l_rej[i]=0;
	m_tpp_acc[i]=0;
	m_tpp_rej[i]=0;
      }
      m_S0_prop*=std::sqrt(float(m_S0_acc+1)/float(m_S0_rej+1));
      m_S0_prop=std::min(m_S0_prop,maxfloat);
      m_S0_acc=0;
      m_S0_rej=0;
    }
    bool compute_l_prior(){
      for(unsigned int i=0;i<m_l.size();i++)
	compute_l_prior(i);
      return false;
    }
    bool compute_l_prior(const int i){
      m_l_old_prior[i]=m_l_prior[i];
      m_l_prior[i]=0;
      if(m_l[i]<0 || m_l[i]>0.01){return true;}
      return false;
    }

    bool compute_tpp_prior(){
      for(unsigned int i=0;i<m_tpp.size();i++)
	compute_tpp_prior(i);
      return false;
    }

    bool compute_tpp_prior(const int& i){
      m_tpp_old_prior[i]=m_tpp_prior[i];
      if(m_tpp[i]==0 || i>0)
	m_tpp_prior[i]=0;
      else
	m_tpp_prior[i]=-std::log(fabs(std::sin(m_tpp[i])/2));
      return false;
    }
    bool compute_S0_prior(){
      m_S0_old_prior=m_S0_prior;
      m_S0_prior=0;
      if(m_S0<0){return true;}
      return false;
    }


    inline void compute_prior(){
      m_old_prior_en=m_prior_en;
      m_prior_en=m_l_prior[0]+m_l_prior[1]+m_l_prior[2]+
	m_tpp_prior[0]+m_tpp_prior[1]+m_tpp_prior[2]+m_S0_prior;
    }


    //Compute model predicted signal, only due to the anisotropic compartment
    void compute_signal(){
       m_Signal_old=m_Signal;
       for (int i = 1; i <= m_bvals.Ncols(); i++){
	 m_Signal(i)=compute_buxton(i);
	 //m_Signal(i)=compute_spinecho(i);
       }
    }


    bool propose_tpp(const int& i){
      m_tpp_old[i]=m_tpp[i];
      m_tpp[i]+=MISCMATHS::normrnd().AsScalar()*m_tpp_prop[i];
      bool rejflag=compute_tpp_prior(i);
      m_V_old=m_V;
      tpp_to_vecs(m_tpp,m_V);
      compute_prior();
      compute_signal();
      return rejflag;
    }
    void accept_tpp(const int&i){
      m_tpp_acc[i]++;
    }
    void reject_tpp(const int& i){
      m_tpp[i]=m_tpp_old[i];
      m_V=m_V_old;
      m_tpp_prior[i]=m_tpp_old_prior[i];
      m_prior_en=m_old_prior_en;
      m_Signal=m_Signal_old;
      m_tpp_rej[i]++;
    }


    bool propose_l(const int& i){
      m_l_old[i]=m_l[i];
      m_l[i]+=MISCMATHS::normrnd().AsScalar()*m_l_prop[i];
       bool rejflag=compute_l_prior(i);
      compute_prior();
      compute_signal();
      return rejflag;
    }
    void accept_l(const int& i){
      m_l_acc[i]++;
    }
    void reject_l(const int& i){
      m_l[i]=m_l_old[i];
      m_l_prior=m_l_old_prior;
      m_prior_en=m_old_prior_en;
      m_Signal=m_Signal_old;
      m_l_rej[i]++;
    }

    bool propose_S0(){
      m_S0_old=m_S0;
      m_S0+=MISCMATHS::normrnd().AsScalar()*m_S0_prop;
      bool rejflag=compute_S0_prior();
      compute_prior();
      return rejflag;
    }


    void accept_S0(){
      m_S0_acc++;
    }

    void reject_S0(){
      m_S0=m_S0_old;
      m_S0_prior=m_S0_old_prior;
      m_prior_en=m_old_prior_en;
      m_S0_rej++;
    }

    void compute_likelihood(){
      m_old_likelihood_en=m_likelihood_en;
      NEWMAT::ColumnVector pred(m_data.Nrows());
      pred=m_S0*m_Signal;
      float sumsquares=(m_data-m_S0*m_Signal).SumSquare();
      m_likelihood_en=(m_data.Nrows()/2.0)*std::log(sumsquares/2.0);
    }
    void compute_energy(){
      m_old_energy=m_energy;
      m_energy=m_prior_en+m_likelihood_en;
    }
    bool test_energy(){
      float tmp=std::exp(m_old_energy-m_energy);
      return (tmp>MISCMATHS::unifrnd().AsScalar());
    }
    void restore_energy(){
      m_prior_en=m_old_prior_en;
      m_likelihood_en=m_old_likelihood_en;
      m_energy=m_old_energy;
    }


    //Function that performs a single MCMC iteration
    void jump(){
      //print();
      if(!propose_S0()){
	compute_prior();
	compute_likelihood();
	compute_energy();
	if(test_energy())
	  accept_S0();
	else{
	  reject_S0();
	  restore_energy();
	}
      }
      else
	reject_S0();

      for(unsigned int i=0;i<3;i++){
	if(!propose_l(i)){
	  compute_prior();
	  compute_likelihood();
	  compute_energy();
	  if(test_energy())
	    accept_l(i);
	  else{
	    reject_l(i);
	    restore_energy();
	  }
	}
	else
	  reject_l(i);

	if(!propose_tpp(i)){
	  compute_prior();
	  compute_likelihood();
	  compute_energy();
	  if(test_energy())
	    accept_tpp(i);
	  else{
	    reject_tpp(i);
	    restore_energy();
	  }
	}
	else
	  reject_tpp(i);


      }
    }

    void print()const{
      std::cout<<" SSFP_DTI"<<std::endl;
      std::cout<<"MD = "<<(m_l[0]+m_l[1]+m_l[2])/3.0<<std::endl;
      std::cout<<"S0 = "<<m_S0<<std::endl;
      std::cout<<"---------"<<std::endl;
    }


    SSFP_DTI& operator=(const SSFP_DTI& rhs){
      m_l=rhs.m_l;
      m_tpp=rhs.m_tpp;
      m_S0=rhs.m_S0;
      m_l_prop=rhs.m_l_prop;
      m_tpp_prop=rhs.m_tpp_prop;
      m_S0_prop=rhs.m_S0_prop;
      m_l_old=rhs.m_l_old;
      m_tpp_old=rhs.m_tpp_old;
      m_S0_old=rhs.m_S0_old;
      m_l_prior=rhs.m_l_prior;
      m_tpp_prior=rhs.m_tpp_prior;
      m_S0_prior=rhs.m_S0_prior;
      m_l_old_prior=rhs.m_l_old_prior;
      m_tpp_old_prior=rhs.m_tpp_old_prior;
      m_S0_old_prior=rhs.m_S0_old_prior;
      m_V=rhs.m_V;
      m_V_old=rhs.m_V_old;
      m_prior_en=rhs.m_prior_en;
      m_old_prior_en=rhs.m_old_prior_en;
      m_l_acc=rhs.m_l_acc;
      m_l_rej=rhs.m_l_rej;
      m_tpp_acc=rhs.m_tpp_acc;
      m_tpp_rej=rhs.m_tpp_rej;
      m_S0_acc=rhs.m_S0_acc;
      m_S0_rej=rhs.m_S0_rej;
      m_Signal=rhs.m_Signal;
      m_Signal_old=rhs.m_Signal_old;

      m_prior_en=rhs.m_prior_en;
      m_old_prior_en=rhs.m_old_prior_en;
      m_likelihood_en=rhs.m_likelihood_en;;
      m_old_likelihood_en=rhs.m_old_likelihood_en;;
      m_energy=rhs.m_energy;;
      m_old_energy=rhs.m_old_energy;;
      //ssfp
      m_B1=rhs.m_B1;
      m_E1=rhs.m_E1;
      m_E2=rhs.m_E2;
      m_gGG=rhs.m_gGG;
      m_cfm=rhs.m_cfm;
      m_sfm=rhs.m_sfm;

      return *this;
    }

  };
}
#endif
