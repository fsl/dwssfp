#!/bin/sh

subjdir=$1
slice=$2
shift
shift
opts=$*


slicezp=`${FSLDIR}/bin/zeropad $slice 4`

if [ `${FSLDIR}/bin/imtest ${subjdir}/T1map` -eq 1 ];then
    opts="$opts --T1=${subjdir}/T1map_slice_$slicezp"
fi
if [ `${FSLDIR}/bin/imtest ${subjdir}/T2map` -eq 1 ];then
    opts="$opts --T2=${subjdir}/T2map_slice_$slicezp"
fi
if [ `${FSLDIR}/bin/imtest ${subjdir}/B1map` -eq 1 ];then
    opts="$opts --B1=${subjdir}/B1map_slice_$slicezp"
fi



BINDIR=${FSLDIR}/bin
#BINDIR=~saad/fsl/src/dwssfp/

${BINDIR}/dtifit_ssfp \
 --data=$subjdir/data_slice_$slicezp\
 --mask=$subjdir/nodif_brain_mask_slice_$slicezp\
 -b $subjdir/bvals -r $subjdir/bvecs \
    -d $subjdir/diffGradDurs \
    -G $subjdir/diffGradAmps \
    -a $subjdir/flipAngles \
    -T $subjdir/TRs \
    --forcedir --logdir=$subjdir.dtifit/diff_slices/data_slice_$slicezp\
    $opts > $subjdir.dtifit/logs/log$slicezp  && echo Done
