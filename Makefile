include $(FSLCONFDIR)/default.mk

PROJNAME = dwssfp
XFILES   = dtifit_ssfp xfibres_ssfp
SCRIPTS  = dwssfp_dtifit dwssfp_dtifit_postproc.sh \
           dwssfp_dtifit_preproc.sh dwssfp_dtifit_single_slice.sh \
           dwssfp_bedpostx dwssfp_bedpostx_postproc.sh \
           dwssfp_bedpostx_preproc.sh dwssfp_bedpostx_single_slice.sh
LIBS    = -lfsl-newimage -lfsl-miscmaths -lfsl-utils -lfsl-NewNifti \
          -lfsl-znz -lfsl-cprob

all: ${XFILES}

dtifit_ssfp: dtifit.o dtifitoptions.o diffmodels.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

xfibres_ssfp: xfibres.o xfibresoptions.o diffmodels.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
