/*  Diffusion model fitting

    Timothy Behrens, Saad Jbabdi, Stam Sotiropoulos  - FMRIB Image Analysis Group

    Copyright (C) 2005 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined (diffmodels_h)
#define diffmodels_h

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include "stdlib.h"
#include "utils/log.h"
#include "utils/tracer_plus.h"
#include "armawrap/newmat.h"
#include "miscmaths/miscmaths.h"
#include "miscmaths/nonlin.h"


#define two_pi 0.636619772
#define f2x(x) (std::tan((x)/two_pi))   //fraction transformation used in the old model 1
#define x2f(x) (std::abs(two_pi*std::atan((x))))

#define f2beta(f) (std::asin(std::sqrt(f))) //fraction transformation used in the new model 1
#define beta2f(beta) (std::pow(std::sin(beta),2.0))
#define d2lambda(d) (std::sqrt(d))     //diffusivity transformation used in the new model 1
#define lambda2d(lambda) (lambda*lambda)

#define bigger(a,b) ((a)>(b)?(a):(b))
#define smaller(a,b) ((a)>(b)?(b):(a))

namespace DTIFIT {


////////////////////////////////////////////////
//       DIFFUSION TENSOR MODEL
////////////////////////////////////////////////

class DTI : public MISCMATHS::NonlinCF{
public:
  DTI(const NEWMAT::ColumnVector& iY,
      const NEWMAT::Matrix& ibvecs,const NEWMAT::Matrix& ibvals){
    Y = iY;
    npts = Y.Nrows();
    m_v1.ReSize(3);
    m_v2.ReSize(3);
    m_v3.ReSize(3);
    bvecs=ibvecs;
    bvals=ibvals;
    form_Amat();
    nparams=7;
  }
  DTI(const NEWMAT::ColumnVector& iY,
      const NEWMAT::Matrix& inAmat):Amat(inAmat){
    Y = iY;
    npts = Y.Nrows();
    m_v1.ReSize(3);
    m_v2.ReSize(3);
    m_v3.ReSize(3);
    nparams=7;
    iAmat = pinv(Amat);
  }
  ~DTI(){}
  void linfit();
  void nonlinfit();
  void calc_tensor_parameters();
  void sort();
  void set_data(const NEWMAT::ColumnVector& data){Y=data;}
  float get_fa()const{return m_fa;}
  float get_md()const{return m_md;}
  float get_s0()const{return m_s0;}
  float get_mo()const{return m_mo;}
  NEWMAT::ColumnVector get_v1()const{return m_v1;}
  NEWMAT::ColumnVector get_v2()const{return m_v2;}
  NEWMAT::ColumnVector get_v3()const{return m_v3;}
  float get_l1()const{return m_l1;}
  float get_l2()const{return m_l2;}
  float get_l3()const{return m_l3;}
  NEWMAT::ColumnVector get_eigen()const{NEWMAT::ColumnVector x(3);x<<m_l1<<m_l2<<m_l3;return x;}
  NEWMAT::ColumnVector get_tensor()const{
    NEWMAT::ColumnVector x(6);
    x << m_tens(1,1)
      << m_tens(2,1)
      << m_tens(3,1)
      << m_tens(2,2)
      << m_tens(3,2)
      << m_tens(3,3);
    return x;
  }
  NEWMAT::ColumnVector get_v(const int& i)const{if(i==1)return m_v1;else if(i==2)return m_v2;else return m_v3;}
  NEWMAT::ColumnVector get_prediction()const;
  NEWMAT::SymmetricMatrix get_covar()const{return m_covar;}
  NEWMAT::ColumnVector get_data()const{return Y;}
  NEWMAT::Matrix get_Amat()const{return Amat;}

  // derivatives of tensor functions w.r.t. tensor parameters
  NEWMAT::ReturnMatrix calc_fa_grad(const NEWMAT::ColumnVector& _tens)const;
  float calc_fa_var()const;
  NEWMAT::ColumnVector calc_md_grad(const NEWMAT::ColumnVector& _tens)const;
  NEWMAT::ColumnVector calc_mo_grad(const NEWMAT::ColumnVector& _tens)const;

  // conversion between rotation matrix and angles
  void rot2angles(const NEWMAT::Matrix& rot,float& th1,float& th2,float& th3)const;
  void angles2rot(const float& th1,const float& th2,const float& th3,NEWMAT::Matrix& rot)const;

  void print()const{
    std::cout << "DTI FIT RESULTS " << std::endl;
    std::cout << "S0   :" << m_s0 << std::endl;
    std::cout << "MD   :" << m_md << std::endl;
    std::cout << "FA   :" << m_fa << std::endl;
    std::cout << "MO   :" << m_mo << std::endl;
    NEWMAT::ColumnVector x(3);
    x=m_v1;
    if(x(3)<0)x=-x;
    float _th,_ph;MISCMATHS::cart2sph(x,_th,_ph);
    std::cout << "TH   :" << _th*180.0/M_PI << " deg" << std::endl;
    std::cout << "PH   :" << _ph*180.0/M_PI << " deg" << std::endl;
    std::cout << "V1   : " << x(1) << " " << x(2) << " " << x(3) << std::endl;
  }
  void form_Amat(){
    Amat.ReSize(bvecs.Ncols(),7);
    NEWMAT::Matrix tmpvec(3,1), tmpmat;
    for( int i = 1; i <= bvecs.Ncols(); i++){
      tmpvec << bvecs(1,i) << bvecs(2,i) << bvecs(3,i);
      tmpmat = tmpvec*tmpvec.t()*bvals(1,i);
      Amat(i,1) = tmpmat(1,1);
      Amat(i,2) = 2*tmpmat(1,2);
      Amat(i,3) = 2*tmpmat(1,3);
      Amat(i,4) = tmpmat(2,2);
      Amat(i,5) = 2*tmpmat(2,3);
      Amat(i,6) = tmpmat(3,3);
      Amat(i,7) = 1;
    }
    iAmat = pinv(Amat);
  }
  void vec2tens(const NEWMAT::ColumnVector& Vec){
    m_tens.ReSize(3);
    m_tens(1,1)=Vec(1);
    m_tens(2,1)=Vec(2);
    m_tens(3,1)=Vec(3);
    m_tens(2,2)=Vec(4);
    m_tens(3,2)=Vec(5);
    m_tens(3,3)=Vec(6);
  }
  void vec2tens(const NEWMAT::ColumnVector& Vec,NEWMAT::SymmetricMatrix& Tens)const{
    Tens.ReSize(3);
    Tens(1,1)=Vec(1);
    Tens(2,1)=Vec(2);
    Tens(3,1)=Vec(3);
    Tens(2,2)=Vec(4);
    Tens(3,2)=Vec(5);
    Tens(3,3)=Vec(6);
  }
  void tens2vec(const NEWMAT::SymmetricMatrix& Tens,NEWMAT::ColumnVector& Vec)const{
    Vec.ReSize(6);
    Vec<<Tens(1,1)<<Tens(2,1)<<Tens(3,1)<<Tens(2,2)<<Tens(3,2)<<Tens(3,3);
  }

  // nonlinear fitting routines
  NEWMAT::ReturnMatrix grad(const NEWMAT::ColumnVector& p)const;
  std::shared_ptr<MISCMATHS::BFMatrix> hess(const NEWMAT::ColumnVector&p,std::shared_ptr<MISCMATHS::BFMatrix> iptr)const;
  double cf(const NEWMAT::ColumnVector& p)const;
  NEWMAT::ReturnMatrix forwardModel(const NEWMAT::ColumnVector& p)const;

  NEWMAT::ColumnVector rotproduct(const NEWMAT::ColumnVector& x,const NEWMAT::Matrix& R)const;
  NEWMAT::ColumnVector rotproduct(const NEWMAT::ColumnVector& x,const NEWMAT::Matrix& R1,const NEWMAT::Matrix& R2)const;
  float anisoterm(const int& pt,const NEWMAT::ColumnVector& ls,const NEWMAT::Matrix& xx)const;

private:
  NEWMAT::Matrix bvecs;
  NEWMAT::Matrix bvals;
  NEWMAT::ColumnVector Y;
  NEWMAT::Matrix Amat,iAmat;
  int npts,nparams;
  NEWMAT::ColumnVector m_v1,m_v2,m_v3;
  float m_l1,m_l2,m_l3;
  float m_fa,m_s0,m_md,m_mo;
  float m_sse;
  NEWMAT::SymmetricMatrix m_tens;
  NEWMAT::SymmetricMatrix m_covar;
};


////////////////////////////////////////////////
//       Partial Volume Models
////////////////////////////////////////////////

// Generic class
class PVM {
public:
  PVM(const NEWMAT::ColumnVector& iY,
      const NEWMAT::Matrix& ibvecs, const NEWMAT::Matrix& ibvals,
      const int& nfibres):Y(iY),bvecs(ibvecs),bvals(ibvals){

    npts    = Y.Nrows();
    nfib    = nfibres;

    MISCMATHS::cart2sph(ibvecs,alpha,beta);

    cosalpha.ReSize(npts);
    sinalpha.ReSize(npts);
    for(int i=1;i<=npts;i++){
      sinalpha(i) = sin(alpha(i));
      cosalpha(i) = cos(alpha(i));
    }

  }
  virtual ~PVM(){}

  // PVM virtual routines
  virtual void fit()  = 0;
  virtual void sort() = 0;
  virtual void print()const = 0;
  virtual void print(const NEWMAT::ColumnVector& p)const = 0;

  virtual NEWMAT::ReturnMatrix get_prediction()const = 0;

protected:
  const NEWMAT::ColumnVector& Y;
  const NEWMAT::Matrix& bvecs;
  const NEWMAT::Matrix& bvals;
  NEWMAT::ColumnVector alpha;
  NEWMAT::ColumnVector sinalpha;
  NEWMAT::ColumnVector cosalpha;
  NEWMAT::ColumnVector beta;

  int   npts;
  int   nfib;

};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Model 1 : mono-exponential (for single shell). Contrained optimization for the diffusivity, fractions and their sum<1
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class PVM_single_c : public PVM, public MISCMATHS::NonlinCF {
public:
   PVM_single_c(const NEWMAT::ColumnVector& iY,
	     const NEWMAT::Matrix& ibvecs, const NEWMAT::Matrix& ibvals,
	     const int& nfibres, bool incl_f0=false):PVM(iY,ibvecs,ibvals,nfibres),m_include_f0(incl_f0){

    if (m_include_f0)
      nparams = nfib*3 + 3;
    else
      nparams = nfib*3 + 2;

    m_f.ReSize(nfib);
    m_th.ReSize(nfib);
    m_ph.ReSize(nfib);
  }
  ~PVM_single_c(){}

  // routines from NonlinCF
  NEWMAT::ReturnMatrix grad(const NEWMAT::ColumnVector& p)const;
  std::shared_ptr<MISCMATHS::BFMatrix> hess(const NEWMAT::ColumnVector&p,std::shared_ptr<MISCMATHS::BFMatrix> iptr)const;
  double cf(const NEWMAT::ColumnVector& p)const;
  NEWMAT::ReturnMatrix forwardModel(const NEWMAT::ColumnVector& p)const;

  // other routines
  void fit();
  void sort();
  void fit_pvf(NEWMAT::ColumnVector& x)const;
  void fix_fsum(NEWMAT::ColumnVector& fs) const;

  void print()const{
    std::cout << "PVM (Single) FIT RESULTS " << std::endl;
    std::cout << "S0   :" << m_s0 << std::endl;
    std::cout << "D    :" << m_d << std::endl;
    for(int i=1;i<=nfib;i++){
      std::cout << "F" << i << "   :" << m_f(i) << std::endl;
      NEWMAT::ColumnVector x(3);
      x << std::sin(m_th(i))*std::cos(m_ph(i)) << std::sin(m_th(i))*std::sin(m_ph(i)) << std::cos(m_th(i));
      if(x(3)<0)x=-x;
      float _th,_ph;MISCMATHS::cart2sph(x,_th,_ph);
      std::cout << "TH" << i << "  :" << _th*180.0/M_PI << " deg" << std::endl;
      std::cout << "PH" << i << "  :" << _ph*180.0/M_PI << " deg" << std::endl;
      std::cout << "DIR" << i << "   : " << x(1) << " " << x(2) << " " << x(3) << std::endl;
    }
    if (m_include_f0)
      std::cout << "F0    :" << m_f0 << std::endl;

    NEWMAT::ColumnVector pred;
    pred=get_prediction();
    std::cout << "pred.t(): " << pred.t() << std::endl;
  }

  //Print the estimates using a vector with the untransformed parameter values
  void print(const NEWMAT::ColumnVector& p)const{
    NEWMAT::ColumnVector f(nfib);

    std::cout << "PARAMETER VALUES " << std::endl;
    std::cout << "S0   :" << p(1) << std::endl;
    std::cout << "D    :" << p(2) << std::endl;
    for(int i=3,ii=1;ii<=nfib;i+=3,ii++){
      f(ii) = beta2f(p(i))*partial_fsum(f,ii-1);
      std::cout << "F" << ii << "   :" << f(ii) << std::endl;
      std::cout << "TH" << ii << "  :" << p(i+1)*180.0/M_PI << " deg" << std::endl;
      std::cout << "PH" << ii << "  :" << p(i+2)*180.0/M_PI << " deg" << std::endl;
    }
    if (m_include_f0)
      std::cout << "F0    :" << beta2f(p(nparams))*partial_fsum(f,nfib);
  }

  //Returns 1-Sum(f_j), 1<=j<=ii. (ii<=nfib)
  //Used for transforming beta to f and vice versa
  float partial_fsum(NEWMAT::ColumnVector& fs, int ii) const{
    float fsum=1.0;
    for(int j=1;j<=ii;j++)
	fsum-=fs(j);
    return fsum;
  }

  float get_s0()const{return m_s0;}
  float get_f0()const{return m_f0;}
  float get_d()const{return m_d;}
  NEWMAT::ColumnVector get_f()const{return m_f;}
  NEWMAT::ColumnVector get_th()const{return m_th;}
  NEWMAT::ColumnVector get_ph()const{return m_ph;}
  float get_f(const int& i)const{return m_f(i);}
  float get_th(const int& i)const{return m_th(i);}
  float get_ph(const int& i)const{return m_ph(i);}
  NEWMAT::ReturnMatrix get_prediction()const;

  // useful functions for calculating signal and its derivatives
  // functions
  float isoterm(const int& pt,const float& _d)const;
  float anisoterm(const int& pt,const float& _d,const NEWMAT::ColumnVector& x)const;
  // 1st order derivatives
  float isoterm_lambda(const int& pt,const float& lambda)const;
  float anisoterm_lambda(const int& pt,const float& lambda,const NEWMAT::ColumnVector& x)const;
  float anisoterm_th(const int& pt,const float& _d,const NEWMAT::ColumnVector& x,const float& _th,const float& _ph)const;
  float anisoterm_ph(const int& pt,const float& _d,const NEWMAT::ColumnVector& x,const float& _th,const float& _ph)const;
  NEWMAT::ReturnMatrix fractions_deriv(const int& nfib, const NEWMAT::ColumnVector& fs, const NEWMAT::ColumnVector& bs) const;

private:
  int   nparams;
  float m_s0;
  float m_d;
  float m_f0;
  NEWMAT::ColumnVector m_f;
  NEWMAT::ColumnVector m_th;
  NEWMAT::ColumnVector m_ph;
  const bool m_include_f0;   //Indicate whether f0 will be used in the model (an unattenuated signal compartment). That will be added as the last parameter
};



///////////////////////////////////////////////////////////////////////////
//       Old Model 1 with no constraints for the sum of fractions
//////////////////////////////////////////////////////////////////////////
// Model 1 : mono-exponential (for single shell)
class PVM_single : public PVM, public MISCMATHS::NonlinCF {
public:
  PVM_single(const NEWMAT::ColumnVector& iY,
	     const NEWMAT::Matrix& ibvecs, const NEWMAT::Matrix& ibvals,
	     const int& nfibres, bool incl_f0=false):PVM(iY,ibvecs,ibvals,nfibres), m_include_f0(incl_f0){

    if (m_include_f0)
      nparams = nfib*3 + 3;
    else
      nparams = nfib*3 + 2;

    m_f.ReSize(nfib);
    m_th.ReSize(nfib);
    m_ph.ReSize(nfib);
  }
  ~PVM_single(){}

  // routines from NonlinCF
  NEWMAT::ReturnMatrix grad(const NEWMAT::ColumnVector& p)const;
  std::shared_ptr<MISCMATHS::BFMatrix> hess(const NEWMAT::ColumnVector&p,std::shared_ptr<MISCMATHS::BFMatrix> iptr)const;
  double cf(const NEWMAT::ColumnVector& p)const;
  NEWMAT::ReturnMatrix forwardModel(const NEWMAT::ColumnVector& p)const;

  // other routines
  void fit();
  void sort();
  void fix_fsum();
  void print()const{
    std::cout << "PVM (Single) FIT RESULTS " << std::endl;
    std::cout << "S0   :" << m_s0 << std::endl;
    std::cout << "D    :" << m_d << std::endl;
    for(int i=1;i<=nfib;i++){
      std::cout << "F" << i << "   :" << m_f(i) << std::endl;
      NEWMAT::ColumnVector x(3);
      x << sin(m_th(i))*cos(m_ph(i)) << sin(m_th(i))*sin(m_ph(i)) << cos(m_th(i));
      if(x(3)<0)x=-x;
      float _th,_ph;MISCMATHS::cart2sph(x,_th,_ph);
      std::cout << "TH" << i << "  :" << _th*180.0/M_PI << " deg" << std::endl;
      std::cout << "PH" << i << "  :" << _ph*180.0/M_PI << " deg" << std::endl;
      std::cout << "DIR" << i << "   : " << x(1) << " " << x(2) << " " << x(3) << std::endl;
    }
  }

  void print(const NEWMAT::ColumnVector& p)const{
    std::cout << "PARAMETER VALUES " << std::endl;
    std::cout << "S0   :" << p(1) << std::endl;
    std::cout << "D    :" << p(2) << std::endl;
    for(int i=3,ii=1;ii<=nfib;i+=3,ii++){
      std::cout << "F" << ii << "   :" << x2f(p(i)) << std::endl;
      std::cout << "TH" << ii << "  :" << p(i+1)*180.0/M_PI << " deg" << std::endl;
      std::cout << "PH" << ii << "  :" << p(i+2)*180.0/M_PI << " deg" << std::endl;
    }
    if (m_include_f0)
      std::cout << "f0    :" << x2f(p(nparams)) << std::endl;
  }

  float get_s0()const{return m_s0;}
  float get_f0()const{return m_f0;}
  float get_d()const{return m_d;}
  NEWMAT::ColumnVector get_f()const{return m_f;}
  NEWMAT::ColumnVector get_th()const{return m_th;}
  NEWMAT::ColumnVector get_ph()const{return m_ph;}
  float get_f(const int& i)const{return m_f(i);}
  float get_th(const int& i)const{return m_th(i);}
  float get_ph(const int& i)const{return m_ph(i);}
  NEWMAT::ReturnMatrix get_prediction()const;

  void set_s0(const float& s0){m_s0=s0;}
  void set_f0(const float& f0){m_f0=f0;}
  void set_d(const float& d){m_d=d;}
  void set_f(const NEWMAT::ColumnVector& f){m_f=f;}
  void set_th_ph(const NEWMAT::Matrix& dyads){
    float th,ph;
    for(int i=1;i<=nfib;i++){
      MISCMATHS::cart2sph(dyads.Row(i).t(),th,ph);
      m_th(i)=th;
      m_ph(i)=ph;
    }
  }


  // useful functions for calculating signal and its derivatives
  // functions
  float isoterm(const int& pt,const float& _d)const;
  float anisoterm(const int& pt,const float& _d,const NEWMAT::ColumnVector& x)const;
  float bvecs_fibre_dp(const int& pt,const float& _th,const float& _ph)const;
  float bvecs_fibre_dp(const int& pt,const NEWMAT::ColumnVector& x)const;
  // 1st order derivatives
  float isoterm_d(const int& pt,const float& _d)const;
  float anisoterm_d(const int& pt,const float& _d,const NEWMAT::ColumnVector& x)const;
  float anisoterm_th(const int& pt,const float& _d,const NEWMAT::ColumnVector& x,const float& _th,const float& _ph)const;
  float anisoterm_ph(const int& pt,const float& _d,const NEWMAT::ColumnVector& x,const float& _th,const float& _ph)const;
  // 2nd order derivatives
  float isoterm_dd(const int& pt,const float& _d)const;
  float anisoterm_dd(const int& pt,const float& _d,const NEWMAT::ColumnVector& x)const;
  float anisoterm_dth(const int& pt,const float& _d,const NEWMAT::ColumnVector& x,const float& _th,const float& _ph)const;
  float anisoterm_dph(const int& pt,const float& _d,const NEWMAT::ColumnVector& x,const float& _th,const float& _ph)const;
  float anisoterm_thth(const int& pt,const float& _d,const NEWMAT::ColumnVector& x,const float& _th,const float& _ph)const;
  float anisoterm_phph(const int& pt,const float& _d,const NEWMAT::ColumnVector& x,const float& _th,const float& _ph)const;
  float anisoterm_thph(const int& pt,const float& _d,const NEWMAT::ColumnVector& x,const float& _th,const float& _ph)const;

private:
  int   nparams;
  float m_s0;
  float m_d;
  float m_f0;
  NEWMAT::ColumnVector m_f;
  NEWMAT::ColumnVector m_th;
  NEWMAT::ColumnVector m_ph;
  const bool m_include_f0;   //Indicate whether f0 will be used in the model (an unattenuated signal compartment)
};



////////////////////////////////////////////////
//       Partial Volume Models
////////////////////////////////////////////////

// Model 2 : non-mono-exponential (for multiple shells)
class PVM_multi : public PVM, public MISCMATHS::NonlinCF {
public:
  PVM_multi(const NEWMAT::ColumnVector& iY,
	    const NEWMAT::Matrix& ibvecs, const NEWMAT::Matrix& ibvals,
	    const int& nfibres,bool incl_f0=false):PVM(iY,ibvecs,ibvals,nfibres),m_include_f0(incl_f0){

    nparams = nfib*3 + 3;

    m_f.ReSize(nfib);
    m_th.ReSize(nfib);
    m_ph.ReSize(nfib);
  }
  ~PVM_multi(){}

  // routines from NonlinCF
  NEWMAT::ReturnMatrix grad(const NEWMAT::ColumnVector& p)const;
  std::shared_ptr<MISCMATHS::BFMatrix> hess(const NEWMAT::ColumnVector&p,std::shared_ptr<MISCMATHS::BFMatrix> iptr)const;
  double cf(const NEWMAT::ColumnVector& p)const;
  NEWMAT::ReturnMatrix forwardModel(const NEWMAT::ColumnVector& p)const;

  // other routines
  void fit();
  void sort();
  void fix_fsum();
  void print()const{
    std::cout << "PVM (MULTI) FIT RESULTS " << std::endl;
    std::cout << "S0    :" << m_s0 << std::endl;
    std::cout << "D     :" << m_d << std::endl;
    std::cout << "D_STD :" << m_d_std << std::endl;
    for(int i=1;i<=nfib;i++){
      std::cout << "F" << i << "    :" << m_f(i) << std::endl;
      NEWMAT::ColumnVector x(3);
      x << sin(m_th(i))*cos(m_ph(i)) << sin(m_th(i))*sin(m_ph(i)) << cos(m_th(i));
      if(x(3)<0)x=-x;
      std::cout << "TH" << i << "   :" << m_th(i) << std::endl;
      std::cout << "PH" << i << "   :" << m_ph(i) << std::endl;
      std::cout << "DIR" << i << "   : " << x(1) << " " << x(2) << " " << x(3) << std::endl;
    }
  }
  void print(const NEWMAT::ColumnVector& p)const{
    std::cout << "PARAMETER VALUES " << std::endl;
    std::cout << "S0    :" << p(1) << std::endl;
    std::cout << "D     :" << p(2) << std::endl;
    std::cout << "D_STD :" << p(3) << std::endl;
    for(int i=3,ii=1;ii<=nfib;i+=3,ii++){
      std::cout << "F" << ii << "    :" << x2f(p(i)) << std::endl;
      std::cout << "TH" << ii << "   :" << p(i+1) << std::endl;
      std::cout << "PH" << ii << "   :" << p(i+2) << std::endl;
    }
  }

  float get_s0()const{return m_s0;}
  float get_d()const{return m_d;}
  float get_d_std()const{return m_d_std;}
  NEWMAT::ColumnVector get_f()const{return m_f;}
  NEWMAT::ColumnVector get_th()const{return m_th;}
  NEWMAT::ColumnVector get_ph()const{return m_ph;}
  float get_f(const int& i)const{return m_f(i);}
  float get_th(const int& i)const{return m_th(i);}
  float get_ph(const int& i)const{return m_ph(i);}

  NEWMAT::ReturnMatrix get_prediction()const;


  void set_s0(const float& s0){m_s0=s0;}
  void set_f0(const float& f0){m_f0=f0;}
  void set_d(const float& d){m_d=d;}
  void set_d_std(const float& d_std){m_d_std=d_std;}
  void set_f(const NEWMAT::ColumnVector& f){m_f=f;}
  void set_th_ph(const NEWMAT::Matrix& dyads){
    float th,ph;
    for(int i=1;i<=nfib;i++){
      MISCMATHS::cart2sph(dyads.Row(i).t(),th,ph);
      m_th(i)=th;
      m_ph(i)=ph;
    }
  }

  // useful functions for calculating signal and its derivatives
  // functions
  float isoterm(const int& pt,const float& _a,const float& _b)const;
  float anisoterm(const int& pt,const float& _a,const float& _b,const NEWMAT::ColumnVector& x)const;
  // 1st order derivatives
  float isoterm_a(const int& pt,const float& _a,const float& _b)const;
  float anisoterm_a(const int& pt,const float& _a,const float& _b,const NEWMAT::ColumnVector& x)const;
  float isoterm_b(const int& pt,const float& _a,const float& _b)const;
  float anisoterm_b(const int& pt,const float& _a,const float& _b,const NEWMAT::ColumnVector& x)const;
  float anisoterm_th(const int& pt,const float& _a,const float& _b,const NEWMAT::ColumnVector& x,const float& _th,const float& _ph)const;
  float anisoterm_ph(const int& pt,const float& _a,const float& _b,const NEWMAT::ColumnVector& x,const float& _th,const float& _ph)const;

private:
  int   nparams;
  float m_s0;
  float m_d;
  float m_d_std;
  NEWMAT::ColumnVector m_f;
  NEWMAT::ColumnVector m_th;
  NEWMAT::ColumnVector m_ph;
  float m_f0;
  const bool m_include_f0;
};

}
#endif
