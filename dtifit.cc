/* DTI model for SSFP Data

    Saad Jbabdi - FMRIB Image Analysis Group

    Copyright (C) 2012 University of Oxford  */

/*  CCOPYRIGHT  */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <math.h>
#include "utils/log.h"
#include "utils/tracer_plus.h"
#include "miscmaths/miscprob.h"
#include "miscmaths/miscmaths.h"
#include "miscmaths/nonlin.h"
#include "newimage/newimageall.h"
#include "stdlib.h"
#include "dti.h"
#include "dtifitoptions.h"
#include "diffmodels.h"

using namespace std;
using namespace DTIFIT;
using namespace Utilities;
using namespace NEWMAT;
using namespace NEWIMAGE;
using namespace MISCMATHS;

#define twopi 6.28318531

////////////////////////////////////////////////
//       Some USEFUL FUNCTIONS
////////////////////////////////////////////////

SymmetricMatrix vec2tens(const ColumnVector& Vec){
  SymmetricMatrix tens(3);
  tens(1,1)=Vec(1);
  tens(2,1)=Vec(2);
  tens(3,1)=Vec(3);
  tens(2,2)=Vec(4);
  tens(3,2)=Vec(5);
  tens(3,3)=Vec(6);
  return tens;
}

Matrix form_Amat(const Matrix& r,const Matrix& b)
{
  Matrix A(r.Ncols(),7);
  Matrix tmpvec(3,1), tmpmat;

  for( int i = 1; i <= r.Ncols(); i++){
    tmpvec << r(1,i) << r(2,i) << r(3,i);
    tmpmat = tmpvec*tmpvec.t()*b(1,i);
    A(i,1) = tmpmat(1,1);
    A(i,2) = 2*tmpmat(1,2);
    A(i,3) = 2*tmpmat(1,3);
    A(i,4) = tmpmat(2,2);
    A(i,5) = 2*tmpmat(2,3);
    A(i,6) = tmpmat(3,3);
    A(i,7) = 1;
  }
  return A;
}



//Performs fitting of the tensor using a precalculated pseudoinverse of the design matrix (Amat_pinv)
//Depending on Amat_pinv, the function performs an OLS or WLS fiting of the DTI model.
void tensor_vec2vals(const ColumnVector& Dvec,DiagonalMatrix& Dd,ColumnVector& evec1,ColumnVector& evec2,ColumnVector& evec3,float& f,float& mode)
{
  SymmetricMatrix tens;
  Matrix Vd;
  DiagonalMatrix Ddsorted(3);
  float mDd, fsquared;

  tens = vec2tens(Dvec);

  EigenValues(tens,Dd,Vd);
  mDd = Dd.Sum()/Dd.Nrows();
  int maxind = Dd(1) > Dd(2) ? 1:2;   //finding max,mid and min eigenvalues
  maxind = Dd(maxind) > Dd(3) ? maxind:3;
  int midind;
  if( (Dd(1)>=Dd(2) && Dd(2)>=Dd(3)) || (Dd(1)<=Dd(2) && Dd(2)<=Dd(3)) ){midind=2;}
  else if( (Dd(2)>=Dd(1) && Dd(1)>=Dd(3)) || (Dd(2)<=Dd(1) && Dd(1)<=Dd(3)) ){midind=1;}
  else {midind=3;}
  int minind = Dd(1) < Dd(2) ? 1:2;   //finding minimum eigenvalue
  minind = Dd(minind) < Dd(3) ? minind:3;
  Ddsorted << Dd(maxind) << Dd(midind) << Dd(minind);
  Dd=Ddsorted;
  evec1 << Vd(1,maxind) << Vd(2,maxind) << Vd(3,maxind);
  evec2 << Vd(1,midind) << Vd(2,midind) << Vd(3,midind);
  evec3 << Vd(1,minind) << Vd(2,minind) << Vd(3,minind);

  float e1=Dd(maxind)-mDd, e2=Dd(midind)-mDd, e3=Dd(minind)-mDd;
  float n = (e1 + e2 - 2*e3)*(2*e1 - e2 - e3)*(e1 - 2*e2 + e3);
  float d = (e1*e1 + e2*e2 + e3*e3 - e1*e2 - e2*e3 - e1*e3);
  d = sqrt(MAX(0, d));
  d = 2*d*d*d;
  mode = MIN(MAX(d ? n/d : 0.0, -1),1);

  //Compute the FA
  float numer=1.5*((Dd(1)-mDd)*(Dd(1)-mDd)+(Dd(2)-mDd)*(Dd(2)-mDd)+(Dd(3)-mDd)*(Dd(3)-mDd));
  float denom=(Dd(1)*Dd(1)+Dd(2)*Dd(2)+Dd(3)*Dd(3));

  if(denom>0) fsquared=numer/denom;
  else fsquared=0;
  if(fsquared>0){f=sqrt(fsquared);}
  else{f=0;}
}




////////////////////////////////////////////
//       MCMC SAMPLE STORAGE
////////////////////////////////////////////


class Samples{
public:
  dtifitOptions& opts;

  Matrix m_S0samples;
  vector<Matrix> m_Tsamples;

  ColumnVector m_mean_S0samples;
  Matrix       m_mean_Tsamples;

  int    m_nsamps;

  Matrix m_matrix2volkey;



  Samples(Matrix matrix2volkey):
    opts(dtifitOptions::getInstance()),m_matrix2volkey(matrix2volkey){

    int count=0;
    int nsamples=0;
    int nvoxels=matrix2volkey.Nrows();

    for(int i=0;i<opts.njumps.value();i++){
      count++;
      if(count==opts.sampleevery.value()){
	count=0;nsamples++;
      }
    }
    m_nsamps=nsamples;

    m_S0samples.ReSize(nsamples,nvoxels);
    m_mean_S0samples.ReSize(nvoxels);
    m_mean_S0samples=0;

    m_Tsamples.clear();
    Matrix tmp(6,nvoxels);tmp=0;
    for(int i=0;i<nsamples;i++){
      m_Tsamples.push_back(tmp);
    }
    m_mean_Tsamples.ReSize(6,nvoxels);
    m_mean_Tsamples=0;

  }


  void record(const SSFP_DTI& dti, int vox, int samp){
    m_S0samples(samp,vox)=dti.get_S0();
    m_Tsamples[samp-1].Column(vox)=dti.get_tensor_as_vec();
  }

  void finish_voxel(int vox){
    for(int i=1;i<=m_nsamps;i++){
      m_mean_S0samples(vox)+=m_S0samples(i,vox);
      m_mean_Tsamples.Column(vox)+=m_Tsamples[i-1].Column(vox);
    }
    m_mean_S0samples(vox) /= float(m_nsamps);
    m_mean_Tsamples.Column(vox) /= float(m_nsamps);
  }


  void save(const volume<float>& mask){

    Log& logger = LogSingleton::getInstance();

    int Nx=mask.xsize(),Ny=mask.ysize(),Nz=mask.zsize();

    volume<float> s0(Nx,Ny,Nz);
    copybasicproperties(mask,s0);

    s0=0;
    int vox=0;
    for(int z=0;z<Nz;z++){
      for(int y=0;y<Ny;y++){
	for(int x=0;x<Nx;x++){
	  if(mask(x,y,z)==0)continue;
	  vox++;
	  s0(x,y,z)=m_mean_S0samples(vox);
	}
      }
    }
    save_volume(s0,logger.appendDir("mean_S0samples"));


    volume<float> l1(Nx,Ny,Nz);
    volume<float> l2(Nx,Ny,Nz);
    volume<float> l3(Nx,Ny,Nz);
    volume<float> MD(Nx,Ny,Nz);
    volume<float> FA(Nx,Ny,Nz);
    volume<float> S0(Nx,Ny,Nz);
    volume<float> MODE(Nx,Ny,Nz);
    volume4D<float> V1(Nx,Ny,Nz,3);
    volume4D<float> V2(Nx,Ny,Nz,3);
    volume4D<float> V3(Nx,Ny,Nz,3);
    copybasicproperties(mask,l1);
    copybasicproperties(mask,l2);
    copybasicproperties(mask,l3);
    copybasicproperties(mask,MD);
    copybasicproperties(mask,FA);
    copybasicproperties(mask,MODE);
    copybasicproperties(mask,V1);
    copybasicproperties(mask,V2);
    copybasicproperties(mask,V3);
    l1=0;l2=0;l3=0;MD=0;MODE=0;FA=0;V1=0;V2=0;V3=0;

    DiagonalMatrix evals(3);
    ColumnVector evec1(3),evec2(3),evec3(3);
    float fa,mode;
    vox=0;
    for(int z=0;z<Nz;z++){
      for(int y=0;y<Ny;y++){
	for(int x=0;x<Nx;x++){
	  if(mask(x,y,z)==0)continue;
	  vox++;
	  ColumnVector Dvec(6);
	  Dvec << m_mean_Tsamples.Column(vox);
	  tensor_vec2vals(Dvec,evals,evec1,evec2,evec3,fa,mode);
	  l1(x,y,z)=evals(1);
	  l2(x,y,z)=evals(2);
	  l3(x,y,z)=evals(3);
	  MD(x,y,z)=(evals(1)+evals(2)+evals(3))/3;
	  FA(x,y,z)=fa;
	  MODE(x,y,z)=mode;
	  V1(x,y,z,0)=evec1(1);
	  V1(x,y,z,1)=evec1(2);
	  V1(x,y,z,2)=evec1(3);
	  V2(x,y,z,0)=evec2(1);
	  V2(x,y,z,1)=evec2(2);
	  V2(x,y,z,2)=evec2(3);
	  V3(x,y,z,0)=evec3(1);
	  V3(x,y,z,1)=evec3(2);
	  V3(x,y,z,2)=evec3(3);

	}
      }
    }

    FA.setDisplayMaximumMinimum(1,0);
    save_volume(FA,logger.appendDir("dti_FA"));
    MODE.setDisplayMaximumMinimum(1,-1);
    save_volume(MODE,logger.appendDir("dti_MO"));
    V1.setDisplayMaximumMinimum(1,-1);
    save_volume4D(V1,logger.appendDir("dti_V1"));
    V2.setDisplayMaximumMinimum(1,-1);
    save_volume4D(V2,logger.appendDir("dti_V2"));
    V3.setDisplayMaximumMinimum(1,-1);
    save_volume4D(V3,logger.appendDir("dti_V3"));
    l1.setDisplayMaximumMinimum(l1.max(),0);
    save_volume(l1,logger.appendDir("dti_L1"));
    l2.setDisplayMaximumMinimum(l1.max(),0);
    save_volume(l2,logger.appendDir("dti_L2"));
    l3.setDisplayMaximumMinimum(l1.max(),0);
    save_volume(l3,logger.appendDir("dti_L3"));
    MD.setDisplayMaximumMinimum(l1.max(),0);
    save_volume(MD,logger.appendDir("dti_MD"));
  }

};


////////////////////////////////////////////
//       MCMC HANDLING
////////////////////////////////////////////



class DtiVoxelManager{

  dtifitOptions& opts;

  Samples& m_samples;
  int m_voxelnumber;
  const ColumnVector m_data;
  const Matrix& m_bvecs;
  const Matrix& m_bvals;

  const Matrix& m_diffGradDur;
  const Matrix& m_diffGradAmp;
  const Matrix& m_TR;
  const Matrix& m_flipAngle;

  SSFP_DTI m_dti;

public:
  DtiVoxelManager(const ColumnVector& data,const Matrix& r,const Matrix& b,
		  const Matrix& _diffGradDur,const Matrix& _diffGradAmp,
		  const Matrix& _TR,const Matrix& _flipAngle,
		  Samples& samples,int voxelnumber,float t1,float t2,float b1):
    opts(dtifitOptions::getInstance()),
    m_samples(samples),m_voxelnumber(voxelnumber),m_data(data),
    m_bvecs(r), m_bvals(b),
    m_diffGradDur(_diffGradDur),m_diffGradAmp(_diffGradAmp),m_TR(_TR),m_flipAngle(_flipAngle),
    m_dti(m_data,m_bvecs,m_bvals,
	  m_diffGradDur,m_diffGradAmp,m_TR,m_flipAngle,
	  t1,t2,b1){

}


  void initialise(float t1,float t2){

      DTI dti(m_data,m_bvecs,m_bvals);
      dti.linfit();
      vector<float> lambdas(3,0.002);
      vector<float> tpp(3,0.0);
      lambdas[0]=dti.get_l1()>0?dti.get_l1():0.002;
      lambdas[1]=dti.get_l2()>0?dti.get_l2():0.001;
      lambdas[2]=dti.get_l3()>0?dti.get_l3():0.0005;
      vector<ColumnVector> V;
      V.push_back(dti.get_v1());
      V.push_back(dti.get_v2());
      V.push_back(dti.get_v3());
      vecs_to_tpp(V,tpp);

      // turn into M0
      float te=28.0/1e3;
      float tr=35.0/1e3;
      float s0=dti.get_s0()/(std::exp(-te/t2)*(1.0-std::exp(-tr/t1)));
      m_dti.initialise_dti(lambdas,tpp,s0);

      m_dti.initialise_energies();
      m_dti.initialise_props();

    }

  void runmcmc(){
    int count=0, recordcount=0,sample=1;//sample will index a newmat matrix
    for( int i =0;i<opts.nburn.value();i++){
      m_dti.jump();
      count++;
      if(count==opts.updateproposalevery.value()){
	m_dti.update_proposals();
	count=0;
      }
    }
    for( int i =0;i<opts.njumps.value();i++){
      m_dti.jump();
      count++;
      recordcount++;
      if(recordcount==opts.sampleevery.value()){
	m_samples.record(m_dti,m_voxelnumber,sample);
	sample++;
	recordcount=0;
      }
      if(count==opts.updateproposalevery.value()){
	m_dti.update_proposals();
	count=0;

      }
    }
    m_samples.finish_voxel(m_voxelnumber);
  }

  ReturnMatrix get_prediction()const{
    ColumnVector pred=m_dti.get_prediction();
    pred.Release();
    return pred;
  }


};




////////////////////////////////////////////
//       MAIN
////////////////////////////////////////////

int main(int argc, char *argv[])
{

    // Setup logging:
    Log& logger = LogSingleton::getInstance();
    dtifitOptions& opts = dtifitOptions::getInstance();
    opts.parse_command_line(argc,argv,logger);
    srand(opts.seed.value());
    Matrix bvals,bvecs,matrix2volkey;
    volume<float> mask;
    volume<int> vol2matrixkey;
    bvals=read_ascii_matrix(opts.bvalsfile.value());
    bvecs=read_ascii_matrix(opts.bvecsfile.value());
    if(bvecs.Nrows()>3) bvecs=bvecs.t();
    if(bvals.Nrows()>1) bvals=bvals.t();
    for(int i=1;i<=bvecs.Ncols();i++){
      float tmpsum=sqrt(bvecs(1,i)*bvecs(1,i)+bvecs(2,i)*bvecs(2,i)+bvecs(3,i)*bvecs(3,i));
      if(tmpsum!=0){
	bvecs(1,i)=bvecs(1,i)/tmpsum;
	bvecs(2,i)=bvecs(2,i)/tmpsum;
	bvecs(3,i)=bvecs(3,i)/tmpsum;
      }
    }

    ColumnVector datam;
    volume4D<float> data;
    read_volume4D(data,opts.datafile.value());
    read_volume(mask,opts.maskfile.value());
    matrix2volkey=data.matrix2volkey(mask);
    vol2matrixkey=data.vol2matrixkey(mask);


    // SSFP
    Matrix diffGradAmp,diffGradDur,TR,flipAngle;
    diffGradAmp=read_ascii_matrix(opts.gradampfile.value());
    diffGradDur=read_ascii_matrix(opts.graddurfile.value());
    TR=read_ascii_matrix(opts.trfile.value());
    flipAngle=read_ascii_matrix(opts.flipanglefile.value());
    flipAngle = flipAngle*M_PI/180.0;


    volume<float> T1map,T2map,B1map;
    if(opts.t1file.value()!=""){
      read_volume(T1map,opts.t1file.value());
    }
    if(opts.t2file.value()!=""){
      read_volume(T2map,opts.t2file.value());
    }
    if(opts.b1file.value()!=""){
      read_volume(B1map,opts.b1file.value());
    }

    Samples samples(matrix2volkey);

    float t1,t2,b1;
    t1=500;
    t2=50;
    b1=1;

    volume4D<float> pred(data.xsize(),data.ysize(),data.zsize(),data.tsize());
    copybasicproperties(data,pred);
    pred=0;
    int vox=0;
    datam.ReSize(data.tsize());
    for(int z=0;z<mask.zsize();z++){
      for(int y=0;y<mask.ysize();y++){
	for(int x=0;x<mask.xsize();x++){
	  if(mask(x,y,z)==0)continue;
	  for(int t=0;t<data.tsize();t++)
	    datam(t+1)=data(x,y,z,t);
	  vox++;
	  cout<<x<<" "<<y<<" "<<z<<endl;


	  if(opts.t1file.value()!="")
	    t1=T1map(x,y,z);
	  if(opts.t2file.value()!="")
	    t2=T2map(x,y,z);
	  if(opts.t2file.value()!="")
	    b1=B1map(x,y,z);


	  DtiVoxelManager  vm(datam,bvecs,bvals,
			      diffGradDur,diffGradAmp,TR,flipAngle,
			      samples,vox,t1/1000.0,t2/1000.0,b1);

	  vm.initialise(t1/1000.0,t2/1000.0);
	  vm.runmcmc();
	  ColumnVector predv=vm.get_prediction();
	  for(int t=0;t<data.tsize();t++)
	    pred(x,y,z,t)=predv(t+1);
	}
      }
    }

    samples.save(mask);
    save_volume4D(pred,logger.appendDir("pred"));

  return 0;


}
